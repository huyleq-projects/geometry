#ifndef CONVEXHULL_H
#define CONVEXHULL_H

#include "geomutil.h"
#include <cassert>
#include <cmath>

// convex hull by jarvis wrap
template<typename T>
std::vector<Point<T>> convexhull_jarvis(const std::vector<Point<T>> &v) {
    if (v.size() < 3) return std::vector<Point<T>>(v.begin(), v.end());

    // find point with min x coordinate and put in hull
    std::vector<Point<T>> hull;
    Point<T> v0 = *std::min_element(v.begin(), v.end(), compare_lexi<T>);
    hull.push_back(v0);

    // incrementally find point with min angle from previously found vertex
    Point<T> v1 = min_a<T>(hull.back(), v.begin(), v.end());
    while (v1 != v0) {
        hull.push_back(v1);
        v1 = min_a<T>(hull.back(), v.begin(), v.end());
    }
    return hull;
}

// convex hull by graham scan
template<typename T>
std::vector<Point<T>> convexhull_graham(const std::vector<Point<T>> &vp) {
    // make a copy because need to sort
    std::vector<Point<T>> v(vp.begin(), vp.end());
    if (vp.size() < 3) return v;

    // find point with min x coordinate and min angle and put in hull
    std::vector<Point<T>> hull;

    Point<T> v0 = *std::min_element(v.begin(), v.end(), compare_lexi<T>);
    hull.push_back(v0);

    sort_a<T>(v0, v.begin(), v.end());
    Point<T> v1 = *v.begin();
    hull.push_back(v1);

    // go around v, test for turn direction and push/pop
    int m = v.size() - 1, i = 1, j = -1;
    Point<T> v2 = *(v.begin() + i);
    while (i != m) {
        double c = cross<T>(v0, v1, v2) ;
        if (c >= 0) { // move forward
            hull.push_back(v2);
            j++; i++;
            v0 = v1; v1 = v2; v2 = *(v.begin() + i);
        }
        else { // roll back
            v1 = v0; v0 = hull[j--];
            hull.pop_back();
        }
    }

    return hull;
}

// convex hull by divide and conquer
template<typename T>
std::vector<Point<T>> convexhull_divideconquer(const std::vector<Point<T>> &vp) {
    assert(vp.size() > 0);
    if (vp.size() < 3) return std::vector<Point<T>>(vp.begin(), vp.end());

    // make a copy because need to sort
    std::vector<Point<T>> v(vp.begin(), vp.end());

    // sort lexicographically
    std::sort(v.begin(), v.end(), compare_lexi<T>);

    // compute the left and right hulls and merge
    std::vector<Point<T>> left(v.begin(), v.begin()+v.size()/2);
    std::vector<Point<T>> lefthull = convexhull_divideconquer<T>(left);

    std::vector<Point<T>> right(v.begin()+v.size()/2, v.end());
    std::vector<Point<T>> righthull = convexhull_divideconquer<T>(right);

    return merge_polygons_leftright(lefthull, righthull);
}

// convex hull by Chan's algorithm. h is a guess for size of convex hull of vp
template<typename T>
std::vector<Point<T>> _convexhull_chan(const std::vector<Point<T>> &vp, int h) {
    // divide vp into groups of more or less h points and find convex hull of each group
    int n = vp.size(); // n = kh + r, r = ak + b
    int k = n/h, r = n%h, a = r/k, b = r%k, j1 = 0;

    std::vector<std::vector<Point<T>>> subhulls;
    for (int i = 0; i < k; i++) {
        // n = b groups of (h+a+1) points + (k-b) groups of (h+a) points
        int j2 = j1 + (i < b? h+a+1: h+a);
        std::vector<Point<T>> v(vp.begin()+j1, vp.begin()+j2);
        subhulls.emplace_back(convexhull_graham(v));
        j1 = j2;
    }

    // find the big convex hull by javis. return empty if h < actual size of hull: bad guess
    std::vector<Point<T>> hull;
    Point<T> v0 = *std::min_element(vp.begin(), vp.end(), compare_lexi<T>);
    hull.push_back(v0); // starting from the lexicographically minium point

    for (int i = 0; i < h; i++) {
        // find the upper and lower tangents from most recent point in hull to all subhulls
        v0 = hull.back();
        std::vector<Point<T>> tangent_points;
        for (const std::vector<Point<T>> &subhull: subhulls) {
            std::pair<int,int> a = upper_lower_tangents(v0, subhull);
            if (a.first != -1 && a.second != -1) {
                if (a.first != a.second) { // point outside of subhull
                    tangent_points.push_back(subhull[a.first]);
                    tangent_points.push_back(subhull[a.second]);
                }
                else { // point on subhull
                    tangent_points.push_back(subhull[mod(a.first+1, subhull.size())]);
                    tangent_points.push_back(subhull[mod(a.first-1, subhull.size())]);
                }
            }
        }

        // find the point of smallest polar angle among tangent points
        Point<T> v1 = min_a<T>(v0, tangent_points.begin(), tangent_points.end());
        if (v1 == hull.front()) return hull; // good guess of h, return found hull
        else hull.push_back(v1);
    }

    // if it gets here, the h < actual size of hull, return empty hull to signal bad guess h
    hull.clear();
    return hull;
}

template<typename T>
std::vector<Point<T>> convexhull_chan(const std::vector<Point<T>> &vp) {
    // start with small guess of h and square up
    int n = vp.size(), h = 2;
    bool found = false;
    while (!found) {
        h = std::min(h*h, n);
        auto hull = _convexhull_chan(vp, h);
        if (hull.empty()) found = false;
        else return hull;
    }

    // if it gets here, something is wrong
    std::cout << "something is wrong with convexhull_chan\n";
    return std::vector<Point<T>>();
}

// convex hull by quick hull
template<typename T>
std::vector<Point<T>> convexhull_quickhull(const std::vector<Point<T>> &v) {
    if (v.size() < 3) return std::vector<Point<T>>(v.begin(), v.end());

    // find points with min and max x coordinates
    Point<T> a = *std::min_element(v.begin(), v.end(), compare_lexi<T>);
    Point<T> b = *std::max_element(v.begin(), v.end(), compare_lexi<T>);

    // divide v into 2 sets above and below ab
    std::vector<Point<T>> above = left_points(a, b, v);
    std::vector<Point<T>> below = left_points(b, a, v);

    // find upper and lower hulls
    std::vector<Point<T>> upper_hull = quickhull_helper(above, a, b);
    std::vector<Point<T>> lower_hull = quickhull_helper(below, b, a);

    // combine
    upper_hull.pop_back(); // remove a
    lower_hull.pop_back(); // remove b
    for (const auto &p: lower_hull) upper_hull.emplace_back(p);
    return upper_hull;
}

template<typename T>
std::vector<Point<T>> quickhull_helper(const std::vector<Point<T>> &v,
                                       const Point<T> &left,
                                       const Point<T> &right) {
    // base cases
    if (v.empty()) return std::vector<Point<T>>{right, left};
    if (v.size() == 1) return std::vector<Point<T>>{right, v[0], left};

    // find the furthest point from left-right segment
    Point<T> h = furthest_points(left, right, v);

    // divide v into 2 sets
    std::vector<Point<T>> v1 = left_points(left, h, v);
    std::vector<Point<T>> v2 = left_points(h, right, v);

    // recurse
    std::vector<Point<T>> hull1 = quickhull_helper(v1, left, h);
    std::vector<Point<T>> hull2 = quickhull_helper(v2, h, right);

    // combine
    hull2.pop_back(); // remove h
    for (const auto &p: hull1) hull2.emplace_back(p);
    return hull2;
}


#endif
