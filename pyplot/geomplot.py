import re

def read_points(filename):
    delimiters = "[", "]", "(", ")", " ", ",", "\n"
    regex_pattern = '|'.join(map(re.escape, delimiters))

    o = []
    f = open(filename, "r")
    lines = f.readlines()
    for line in lines:
        if line[0] == '#': continue
        tokens = re.split(regex_pattern, line)
        tokens = [float(x) for x in tokens if x]
        o.append(tokens)

    return o

class PlotParams:
    def __init__(self, filename, color, labeling):
        self.filename = filename
        self.color = color
        self.labeling = labeling

def plot_points(ax, params):
    pointlist = read_points(params.filename)
    for points in pointlist:
        ax.scatter(points[0::2], points[1::2], c=params.color)
        if params.labeling:
            for i in range(0, len(points), 2):
                s = str(int(i/2)) + '(' + str(points[i]) + ', ' + str(points[i+1]) + ')'
                ax.text(points[i], points[i+1], s, size='small')

def plot_segments(ax, params):
    pointlist = read_points(params.filename)
    for points in pointlist:
        for i in range(0, len(points), 4):
            ax.plot([points[i], points[i+2]], [points[i+1], points[i+3]], c=params.color)

            if params.labeling:
                s = str(int(i/4)) # edge label
                ax.text((points[i] + points[i+2])/2, (points[i+1] + points[i+3])/2, s, size='small')

                s = '(' + str(points[i]) + ', ' + str(points[i+1]) + ')'
                ax.text(points[i], points[i+1], s, size='small')

                s = '(' + str(points[i+2]) + ', ' + str(points[i+3]) + ')'
                ax.text(points[i+2], points[i+3], s, size='small')

def plot_polygons(ax, params):
    pointlist = read_points(params.filename)
    for points in pointlist:
        points.append(points[0]); points.append(points[1])
        ax.plot(points[0::2], points[1::2], c=params.color)
        if params.labeling:
            for i in range(0, len(points), 2):
                s = str(int(i/2)) + '(' + str(points[i]) + ', ' + str(points[i+1]) + ')'
                ax.text(points[i], points[i+1], s, size='small')

