#include "geomutil.h"
#include <gtest/gtest.h>
#include <iostream>
#include <algorithm>

TEST(TestUtil, ClassifyPolygon) {
    // no vertices
    std::vector<Point2i> v0;
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::ConvexDegenerate);

    std::cout << "1 vertex\n";
    Point2i o(1, 1);
    v0.push_back(o);
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::ConvexDegenerate);

    std::cout << "2 vertices\n";
    v0.push_back(o);
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::ConvexDegenerate);

    std::cout << "3 same vertices\n";
    v0.push_back(o);
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::ConvexDegenerate);

    std::cout << "3 same vertices and a different fourth\n";
    v0.emplace_back(-1, -1);
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::ConvexDegenerate);

    std::cout << "1 vertex and 3 same vertices\n";
    std::reverse(v0.begin(), v0.end());
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::ConvexDegenerate);

    std::cout << "colinear vertices\n";
    v0.emplace_back(0, 0);
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::ConvexDegenerate);

    std::cout << "colinear vertices with change of direction\n";
    v0.emplace_back(2, 2);
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::NonConvexDegenerate);

    std::cout << "nonconvex vertices with change of direction\n";
    v0.back() = Point2i(2, 1);
    EXPECT_TRUE(classify_polygon<int>(v0) == ConvexPolygonClass::NonConvex);

    std::cout << "convex ccw with duplicate consecutive vertices\n";
    std::vector<Point2i> v1;
    v1.emplace_back(-1, -1); v1.emplace_back(-1, -1);
    v1.emplace_back( 0, -1); v1.emplace_back( 0, -1);
    v1.emplace_back( 1, -1); v1.emplace_back( 1, -1);
    v1.emplace_back( 1,  0); v1.emplace_back( 1,  0);
    v1.emplace_back( 1,  1); v1.emplace_back( 1,  1);
    v1.emplace_back( 0,  1); v1.emplace_back( 0,  1);
    v1.emplace_back(-1,  1); v1.emplace_back(-1,  1);
    v1.emplace_back(-1,  0); v1.emplace_back(-1,  0);
    EXPECT_TRUE(classify_polygon<int>(v1) == ConvexPolygonClass::ConvexCCW);

    std::cout << "convex ccw with duplicate consecutive vertices that wrap around twice\n";
    v1.emplace_back(-1, -1); v1.emplace_back(-1, -1);
    v1.emplace_back( 0, -1); v1.emplace_back( 0, -1);
    v1.emplace_back( 1, -1); v1.emplace_back( 1, -1);
    v1.emplace_back( 1,  0); v1.emplace_back( 1,  0);
    v1.emplace_back( 1,  1); v1.emplace_back( 1,  1);
    v1.emplace_back( 0,  1); v1.emplace_back( 0,  1);
    v1.emplace_back(-1,  1); v1.emplace_back(-1,  1);
    v1.emplace_back(-1,  0); v1.emplace_back(-1,  0);
    EXPECT_TRUE(classify_polygon<int>(v1) == ConvexPolygonClass::NonConvex);

    std::cout << "convex cw with duplicate consecutive vertices\n";
    std::vector<Point2i> v2;
    v2.emplace_back(-1, -1); v2.emplace_back(-1, -1);
    v2.emplace_back(-1,  0); v2.emplace_back(-1,  0);
    v2.emplace_back(-1,  1); v2.emplace_back(-1,  1);
    v2.emplace_back( 0,  1); v2.emplace_back( 0,  1);
    v2.emplace_back( 1,  1); v2.emplace_back( 1,  1);
    v2.emplace_back( 1,  0); v2.emplace_back( 1,  0);
    v2.emplace_back( 1, -1); v2.emplace_back( 1, -1);
    v2.emplace_back( 0, -1); v2.emplace_back( 0, -1);
    EXPECT_TRUE(classify_polygon<int>(v2) == ConvexPolygonClass::ConvexCW);

    std::cout << "convex cw with duplicate consecutive vertices that wrap around twice\n";
    v2.emplace_back(-1, -1); v2.emplace_back(-1, -1);
    v2.emplace_back(-1,  0); v2.emplace_back(-1,  0);
    v2.emplace_back(-1,  1); v2.emplace_back(-1,  1);
    v2.emplace_back( 0,  1); v2.emplace_back( 0,  1);
    v2.emplace_back( 1,  1); v2.emplace_back( 1,  1);
    v2.emplace_back( 1,  0); v2.emplace_back( 1,  0);
    v2.emplace_back( 1, -1); v2.emplace_back( 1, -1);
    v2.emplace_back( 0, -1); v2.emplace_back( 0, -1);
    EXPECT_TRUE(classify_polygon<int>(v2) == ConvexPolygonClass::NonConvex);

    std::cout << "nonconvex with duplicate consecutive vertices and change of direction\n";
    std::vector<Point2i> v3;
    v3.emplace_back(-2, -2); v3.emplace_back(-2, -2);
    v3.emplace_back(-2,  1); v3.emplace_back(-2,  1);
    v3.emplace_back(-2,  0); v3.emplace_back(-2,  0);
    v3.emplace_back(-2,  2); v3.emplace_back(-2,  2);
    v3.emplace_back( 0,  2); v3.emplace_back( 0,  2);
    v3.emplace_back( 2,  2); v3.emplace_back( 2,  2);
    v3.emplace_back( 2,  0); v3.emplace_back( 2,  0);
    v3.emplace_back( 2, -2); v3.emplace_back( 2, -2);
    v3.emplace_back( 0, -2); v3.emplace_back( 0, -2);
    EXPECT_TRUE(classify_polygon<int>(v3) == ConvexPolygonClass::NonConvex);

    std::cout << "nonconvex with duplicate nonconsecutive vertices\n";
    std::vector<Point2i> v4;
    v4.emplace_back(-2, 0);
    v4.emplace_back( 0, 2);
    v4.emplace_back( 0, 3);
    v4.emplace_back( 0, 2);
    v4.emplace_back( 2, 0);
    EXPECT_TRUE(classify_polygon<int>(v4) == ConvexPolygonClass::NonConvex);

    std::cout << "nonconvex with duplicate nonconsecutive vertices\n";
    std::vector<Point2i> v5;
    v5.emplace_back(-2, 0);
    v5.emplace_back( 0, 2);
    v5.emplace_back( 0, 1);
    v5.emplace_back( 0, 2);
    v5.emplace_back( 2, 0);
    EXPECT_TRUE(classify_polygon<int>(v5) == ConvexPolygonClass::NonConvex);

    std::cout << "star shape with duplicate consecutive vertices\n";
    std::vector<Point2i> v6;
    v6.emplace_back(-1, -1); v6.emplace_back(-1, -1);
    v6.emplace_back( 0,  2); v6.emplace_back( 0,  2);
    v6.emplace_back( 1, -1); v6.emplace_back( 1, -1);
    v6.emplace_back(-1,  1); v6.emplace_back(-1,  1);
    v6.emplace_back( 1,  1); v6.emplace_back( 1,  1);
    EXPECT_TRUE(classify_polygon<int>(v6) == ConvexPolygonClass::NonConvex);

    std::cout << "convex ccw starting at non-lexicographically minimum vertex\n";
    std::vector<Point2i> v7;
    v7.emplace_back( 0,  2); v7.emplace_back( 0,  2);
    v7.emplace_back(-1,  1); v7.emplace_back(-1,  1);
    v7.emplace_back(-1, -1); v7.emplace_back(-1, -1);
    v7.emplace_back( 1, -1); v7.emplace_back( 1, -1);
    v7.emplace_back( 1,  1); v7.emplace_back( 1,  1);
    EXPECT_TRUE(classify_polygon<int>(v7) == ConvexPolygonClass::ConvexCCW);
}
