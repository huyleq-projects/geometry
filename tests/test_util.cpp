#include "geomutil.h"
#include <gtest/gtest.h>
#include <iostream>
#include <string>
#include <chrono>
#include <random>
#include <algorithm>

TEST(TestUtil, Simple) {
    Point2i o(-1, -1), a(1, 2), b(2, 1), c(0, 2), d(3, -3);
    Point2d x(a);
    EXPECT_TRUE(x == a);

    // test cross product
    EXPECT_TRUE(cross(o, a, b) == -5);

    // test io
    std::vector<Point2i> vp;
    vp.push_back(b);
    vp.push_back(a);
    vp.push_back(o);
    vp.push_back(c);
    vp.push_back(d);
    std::cout << "a vector of points\n";
    print(vp.begin(), vp.end());
    std::string filename = "./input.txt";
    write_to_file(filename, vp.begin(), vp.end());

    std::vector<Point2i> v0 = read_points_from_file<int>("thisfiledoesnotexist.txt");
    std::vector<Point2i> v = read_points_from_file<int>(filename);
    EXPECT_TRUE(v.size() > 0);
    std::cout << "in input.txt\n";
    EXPECT_TRUE(v == vp);

    // test min elements and sorting
    Point2i minx = *std::min_element(vp.begin(), vp.end(), compare_lexi<int>);
    std::cout << "minx = " << minx << std::endl;
    EXPECT_TRUE(minx == o);

    Point2i mina = min_a<int>(minx, vp.begin(), vp.end());
    std::cout << "mina = " << mina << std::endl;
    EXPECT_TRUE(mina == d);

    sort_a<int>(minx, vp.begin(), vp.end());
    std::cout << "after sorting by angle\n";
    print(vp.begin(), vp.end());
    EXPECT_TRUE(vp[0] == d
             && vp[1] == b
             && vp[2] == a
             && vp[3] == c
             && vp[4] == o);
}

bool test_sort(std::vector<Point2i> &vv) {
    std::cout << "before sorting\n";
    print(vv.begin(), vv.end());

    Point2i o = *std::min_element(vv.begin(), vv.end(), compare_lexi<int>);
    auto cmp = [&o] (const Point2i &a, const Point2i &b) {
        return compare_angle<int>(o, a, b);
    };
    std::sort(vv.begin(), vv.end(), cmp);

    std::cout << "after sorting\n";
    print(vv.begin(), vv.end());
    return (std::is_sorted(vv.begin(), vv.end(), cmp));
}

TEST(TestUtil, Sorting) {
    // test sorting with input from file
    std::vector<Point2i> vv;
    vv.emplace_back(5414, 3051);
    vv.emplace_back(1630, 8462);
    vv.emplace_back(9055, 4222);
    vv.emplace_back(4587, 5182);
    vv.emplace_back(9294, 7485);
    vv.emplace_back(9871, 7726);
    vv.emplace_back(1026, 108);
    EXPECT_TRUE(test_sort(vv));

    // make a random list of points
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(3, 100);

    int n = distribution(generator);
    std::vector<Point2i> vp = random_ipoints(n, -10000, 10000);
    EXPECT_TRUE(test_sort(vp));
}

TEST(TestUtil, TangentLeftRight) {
    // coordinates
    std::vector<int> x{-3, -5, -4, -2, -1};
    std::vector<int> y{ 1, -1, -3, -2,  0};

    // shifts applied to first polygon to create the second
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(0, 1000);
    int dx = distribution(generator), dy = distribution(generator);

    // create 2 polygons
    std::vector<Point2i> leftpoly, rightpoly;
    for (int i = 0; i < x.size(); i++) {
        leftpoly.emplace_back(x[i], y[i]);
        rightpoly.emplace_back(x[i] + 6 + dx, y[i] + dy);
    }

    std::string filename = "./leftpoly.txt";
    write_to_file(filename, leftpoly.begin(), leftpoly.end());
    filename = "./rightpoly.txt";
    write_to_file(filename, rightpoly.begin(), rightpoly.end());

    // upper tangent
    auto upper = upper_tangent_leftright<int>(leftpoly, rightpoly);
    EXPECT_TRUE(is_upper_tangent<int>(upper.first, upper.second, leftpoly, rightpoly));

    // lower tangent
    auto lower = lower_tangent_leftright<int>(leftpoly, rightpoly);
    EXPECT_TRUE(is_lower_tangent<int>(lower.first, lower.second, leftpoly, rightpoly));

    // merge
    auto hull = merge_polygons_leftright<int>(leftpoly, rightpoly);
    EXPECT_TRUE(classify_polygon<int>(hull) == ConvexPolygonClass::ConvexCCW);
}

TEST(TestUtil, MergePolygonsLeftRight) {
    // create 2 polygons
    std::vector<Point2i> leftpoly0, rightpoly0;
    leftpoly0.emplace_back(0, 0);
    leftpoly0.emplace_back(1, 1);
    rightpoly0.emplace_back(3, 100);

    // merge
    auto hull0 = merge_polygons_leftright<int>(leftpoly0, rightpoly0);
    EXPECT_TRUE(classify_polygon<int>(hull0) == ConvexPolygonClass::ConvexCCW);

    // create 2 polygons
    std::vector<Point2i> leftpoly1, rightpoly1;
    leftpoly1.emplace_back(0, 0);
    rightpoly1.emplace_back(3, 100);
    rightpoly1.emplace_back(3, 101);

    // merge
    auto hull1 = merge_polygons_leftright<int>(leftpoly1, rightpoly1);
    EXPECT_TRUE(classify_polygon<int>(hull1) == ConvexPolygonClass::ConvexCCW);

    // create 2 polygons
    std::vector<Point2i> leftpoly2, rightpoly2;
    leftpoly2.emplace_back(0, 0);
    rightpoly2.emplace_back(1, 1);
    rightpoly2.emplace_back(3, 101);
    rightpoly2.emplace_back(4, 101);

    // merge
    auto hull2 = merge_polygons_leftright<int>(leftpoly2, rightpoly2);
    EXPECT_TRUE(classify_polygon<int>(hull2) == ConvexPolygonClass::ConvexCCW);
}

TEST(TestUtil, SegmentsIntersect) {
    Segment2i a(0, 1, 10, 1);

    Segment2i b1(-3, 1, -1, 1);
    EXPECT_TRUE(!segments_intersect(a, b1));

    Segment2i b2(-3, 1, 4, 1);
    EXPECT_TRUE(segments_intersect(a, b2));

    Segment2i b3(3, 1, 4, 1);
    EXPECT_TRUE(segments_intersect(a, b3));

    Segment2i b4(3, 1, 14, 1);
    EXPECT_TRUE(segments_intersect(a, b4));

    Segment2i b5(13, 1, 14, 1);
    EXPECT_TRUE(!segments_intersect(a, b5));

    Segment2i b6(1, 3, -3, -1);
    EXPECT_TRUE(!segments_intersect(a, b6));

    Segment2i b7(10, -6, -1, -7);
    EXPECT_TRUE(!segments_intersect(a, b7));

    Segment2i b8(10, 6, -1, 7);
    EXPECT_TRUE(!segments_intersect(a, b8));

    Segment2i b9(1, -6, 9, 7);
    EXPECT_TRUE(segments_intersect(a, b9));

    Segment2i x(1, 1, 3, 2);
    Segment2i y(0, 2, 3, 13);
    EXPECT_TRUE(!segments_intersect(x, y));
}

TEST(TestUtil, SegmentsIntersection) {
    Segment2i a(1, 1, 5, 2);
    Point2d p;

    Segment2i b0(8, -3, 4, 1);
    EXPECT_TRUE(segments_intersection(a, b0) == SegmentIntersectionType::None);

    Segment2i b1(13, 4, 9, 3);
    EXPECT_TRUE(segments_intersection(a, b1) == SegmentIntersectionType::None);

    Segment2i b2(5, 2, 9, 3);
    EXPECT_TRUE(segments_intersection(a, b2, &p) == SegmentIntersectionType::Overlapping);
    EXPECT_TRUE(p.x == 5 && p.y == 2);

    Segment2i b3(9, 3, -3, 0);
    EXPECT_TRUE(segments_intersection(a, b3, &p) == SegmentIntersectionType::Overlapping);
    EXPECT_TRUE(p.x == 1 && p.y == 1);

    Segment2i b4(6, 1, 4, 3);
    EXPECT_TRUE(segments_intersection(a, b4, &p) == SegmentIntersectionType::EndPointTouch);
    EXPECT_TRUE(p.x == 5 && p.y == 2);

    Segment2i b5(4, 0, 2, 2);
    EXPECT_TRUE(segments_intersection(a, b5, &p) == SegmentIntersectionType::Crossing);
    EXPECT_TRUE(is_zero_double(cross(a.p1, a.p2, p)) && on_segment(a, p));
    EXPECT_TRUE(is_zero_double(cross(b5.p1, b5.p2, p)) && on_segment(b5, p));
}

TEST(TestUtil, Area) {
    Point2i a(1, 1), b(2, 1), c(2, 2), d(1, 2);
    std::vector<Point2i> v1;
    v1.push_back(a); v1.push_back(b); v1.push_back(c);
    EXPECT_TRUE((area(v1) - 0.5) < 1e-16);
    v1.push_back(d);
    EXPECT_TRUE((area(v1) - 1) < 1e-16);
    std::reverse(v1.begin(), v1.end());
    EXPECT_TRUE((area(v1) + 1) < 1e-16);
}
