#include "dcel.h"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestDcel, Simple) {
    DCELVertex2i v1(1, 2), v2(2, 1);
    std::cout << v1 << " " << v2<< std::endl;

    DCELHalfEdge2i e1, e2;
    e1.origin = &v1; e2.origin = &v2;
    e1.twin = &e2; e2.twin = &e1;
    std::cout << e1 << " " << e2<< std::endl;

    Point2i a(1, 1), b(4, 1), c(4, 2), d(2, 2), e(2, 4), f(1, 4);

    std::vector<Point2i> points;
    points.push_back(a);
    points.push_back(b);
    points.push_back(c);
    points.push_back(d);
    points.push_back(e);
    points.push_back(f);

    DCEL2i dcel(points);
    std::cout << dcel << std::endl;
    EXPECT_TRUE(dcel.faces.size() == 2);

    DCELFace2i *face = find_face(dcel, a, b);
    EXPECT_TRUE(split_face(dcel, face, a, d));
    std::cout << dcel << std::endl;
    EXPECT_TRUE(dcel.faces.size() == 3);

    face = find_face(dcel, b, c);
    EXPECT_TRUE(split_face(dcel, face, d, b));
    std::cout << dcel << std::endl;
    EXPECT_TRUE(dcel.faces.size() == 4);

    dcel.write_to_file("dcel.txt");

    dcel.clean();
}
