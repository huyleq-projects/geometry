#ifndef GEOMUTIL_H
#define GEOMUTIL_H

#include <cstdio>
#include <cmath>
#include <fstream>
#include <string>
#include <algorithm>
#include <utility>
#include <cassert>

#include "geometry.h"

// distance between 2 points
template<typename Ta, typename Tb=Ta>
double distance(const Point<Ta> &a, const Point<Tb> &b) {
    double dx = double(a.x) - double(b.x), dy = double(a.y) - double(b.y);
    return std::sqrt(dx*dx + dy*dy);
}

// cross product of vectors p0p1 and p0p2
// positive (negative) when p1 to p2 is ccw/left-turning (cw/right-turning) around p0
template<typename T0, typename T1=T0, typename T2=T0>
double cross(const Point<T0> &p0, const Point<T1> &p1, const Point<T2> &p2) {
    return det(double(p1.x) - double(p0.x), double(p1.y) - double(p0.y),
               double(p2.x) - double(p0.x), double(p2.y) - double(p0.y));
}

// find y such that 3 points (x, y), (xi, yi) are colinear
// assume x1 != x2
template<typename T1, typename T2=T1>
double colinear_y(const Point<T1> &a1, const Point<T2> &a2, double x) {
    return colinear_y(double(a1.x), double(a1.y), double(a2.x), double(a2.y), x);
}

template<typename T>
double colinear_y(const Segment<T> &ab, double x) {
    return colinear_y(ab.p1, ab.p2, x);
}

// print turn direction. mostly for debug
template<typename T0, typename T1=T0, typename T2=T0>
void turn_direction(const Point<T0> &p0, const Point<T1> &p1, const Point<T2> &p2) {
    double c = cross(p0, p1, p2);
    if      (c > 0) std::cout << p0 << " " << p1 << " " << p2 << " turn CCW\n";
    else if (c < 0) std::cout << p0 << " " << p1 << " " << p2 << " turn CW\n";
    else            std::cout << p0 << " " << p1 << " " << p2 << " are colinear\n";
    return;
}

// lexicographical comparison
template<typename Ta, typename Tb=Ta>
bool compare_lexi(const Point<Ta> &a, const Point<Tb> &b) {
    return (a.x < b.x || (a.x == b.x && a.y < b.y));
}

template<class PointType>
bool is_higher(const PointType &a, const PointType &b) {
    return (a.y > b.y || (a.y == b.y && a.x < b.x));
}

// find bbox
template<typename T>
std::pair<Point<T>, Point<T>> bbox(const std::vector<Point<T>> &vp) {
    assert(vp.size() > 0);
    Point<T> ll = vp[0], ur = vp[0];
    for (const auto &p: vp) {
        if (p.x < ll.x) ll.x = p.x;
        if (p.x > ur.x) ur.x = p.x;
        if (p.y < ll.y) ll.y = p.y;
        if (p.y > ur.y) ur.y = p.y;
    }
    return std::make_pair(ll, ur);
}

// compare the angles oa and ob make with the x axis
// if a and b are colinear, further point is "smaller"
template<typename To, typename Ta=To, typename Tb=To>
bool compare_angle(const Point<To> &o, const Point<Ta> &a, const Point<Tb> &b) {
    double c = cross(o, b, a);
    return (c < 0 || (c == 0 && distance(o, a) > distance(o, b)));
}

// return the point that makes smallest angle with positive x axis
// or the farthest if there are multiple such points
template<typename T>
Point<T> min_a(const Point<T> &o,
               typename std::vector<Point<T>>::const_iterator first,
               typename std::vector<Point<T>>::const_iterator last) {
    auto cmp = [&o] (const Point<T> &a, const Point<T> &b) {
        return compare_angle<T>(o, a, b);
    };
    return *std::min_element(first, last, cmp);
}

// sort by angles with the positive x axis
template<typename T>
void sort_a(const Point<T> &o,
            typename std::vector<Point<T>>::iterator first,
            typename std::vector<Point<T>>::iterator last) {
    auto cmp = [&o] (const Point<T> &a, const Point<T> &b) {
        return compare_angle<T>(o, a, b);
    };
    std::sort(first, last, cmp);
    return;
}

template<typename T>
bool is_sorted_a(const Point<T> &o,
                 typename std::vector<Point<T>>::const_iterator first,
                 typename std::vector<Point<T>>::const_iterator last) {
    auto cmp = [&o] (const Point<T> &a, const Point<T> &b) {
        return compare_angle<T>(o, a, b);
    };
    return std::is_sorted(first, last, cmp);
}

// return a vector of n random integer points
std::vector<Point2i> random_ipoints(int n, int a=-1e6, int b=1e6);
std::vector<Point2i> random_test_points(int n = 0);

// return a vector of n random integer segments
std::vector<Segment2i> random_isegments(int n, int a=-1e6, int b=1e6);
std::vector<Segment2i> random_test_segments(int n=0);

// return a random polygon
std::vector<Point2i> random_ipolygon(int n = 0);

// read file of points one line each (x1, y1)...
template<typename T>
std::vector<Point<T>> read_points_from_file(const std::string &filename) {
    std::vector<Point<T>> v;
    std::ifstream ifs(filename);
    if (ifs.is_open()) {
        std::string line;
        while (getline(ifs, line)) {
            if (line[0] == '#') continue;
            int p0 = line.find("("), p1 = line.find(",", p0);
            T x = T(std::stof(line.substr(p0+1, p1-p0-1)));
            p0 = line.find(" ", p1); p1 = line.find(")", p0);
            T y = T(std::stof(line.substr(p0+1, p1-p0-1)));
            v.emplace_back(x, y);
        }
        ifs.close();
    }
    else perror("read_points_from_file error");
    return v;
}

// read file of segments one line each [(x1, y1)(x2, y2)]...
template<typename T>
std::vector<Segment<T>> read_segments_from_file(const std::string &filename) {
    std::vector<Segment<T>> v;
    std::ifstream ifs(filename);
    if (ifs.is_open()) {
        std::string line;
        while (getline(ifs, line)) {
            if (line[0] == '#') continue;
            int p0 = line.find("("), p1 = line.find(",", p0);
            T x1 = T(std::stof(line.substr(p0+1, p1-p0-1)));
            p0 = line.find(" ", p1); p1 = line.find(")", p0);
            T y1 = T(std::stof(line.substr(p0+1, p1-p0-1)));
            p0 = line.find("(", p1), p1 = line.find(",", p0);
            T x2 = T(std::stof(line.substr(p0+1, p1-p0-1)));
            p0 = line.find(" ", p1); p1 = line.find(")", p0);
            T y2 = T(std::stof(line.substr(p0+1, p1-p0-1)));
            v.emplace_back(x1, y1, x2, y2);
        }
        ifs.close();
    }
    else perror("read_segments_from_file error");
    return v;
}

// find the upper tangent of 2 convex polygons that are separable by a vertical line
// return a pair of indices in the left and right polygons
template<typename T>
std::pair<int, int> upper_tangent_leftright(const std::vector<Point<T>> &leftpoly,
                                            const std::vector<Point<T>> &rightpoly) {
    // find leftpoly's rightmost point and rightpoly's leftmost point
    int left = std::max_element(leftpoly.begin(), leftpoly.end(), compare_lexi<T>) - leftpoly.begin();
    int right = std::min_element(rightpoly.begin(), rightpoly.end(), compare_lexi<T>) - rightpoly.begin();

    // define the vertical line and find y of its intersection with segment left_right
    double x = (double(leftpoly[left].x) + double(rightpoly[right].x))/2;
    double y = colinear_y<T>(leftpoly[left], rightpoly[right], x);

    // next points on left and right polygons
    int n_left = leftpoly.size(), n_right = rightpoly.size();
    int next_left = mod(left+1, n_left), next_right = mod(right-1, n_right);

    // intersections with vertical lines of left_nextright and nextleft_right segments
    double y_left = colinear_y<T>(leftpoly[next_left], rightpoly[right], x);
    double y_right = colinear_y<T>(leftpoly[left], rightpoly[next_right], x);

    while (y_left > y || y_right > y) { // until the y intercept is maximum
        if (y_left > y) { // advance left ccw
            left = next_left;
            next_left = mod(left+1, n_left);
            y = y_left;
            y_left = colinear_y<T>(leftpoly[next_left], rightpoly[right], x);
            y_right = colinear_y<T>(leftpoly[left], rightpoly[next_right], x);
        }
        else { // advance right cw
            right = next_right;
            next_right = mod(right-1, n_right);
            y = y_right;
            y_right = colinear_y<T>(leftpoly[left], rightpoly[next_right], x);
            y_left = colinear_y<T>(leftpoly[next_left], rightpoly[right], x);
        }
    }
    return std::make_pair(left, right);
}

// test for upper tangency
template<typename T>
bool is_upper_tangent(int left,
                      int right,
                      const std::vector<Point<T>> &leftpoly,
                      const std::vector<Point<T>> &rightpoly) {
    int next_left = mod(left+1, leftpoly.size());
    int prev_left = mod(left-1, leftpoly.size());
    int next_right = mod(right+1, rightpoly.size());
    int prev_right = mod(right-1, rightpoly.size());
    return (cross(leftpoly[left], rightpoly[right], rightpoly[next_right]) <= 0
         && cross(leftpoly[left], rightpoly[right], rightpoly[prev_right]) <= 0
         && cross(rightpoly[right], leftpoly[left], leftpoly[next_left]) >= 0
         && cross(rightpoly[right], leftpoly[left], leftpoly[prev_left]) >= 0);
}

// find the lower tangle of 2 convex polygons that are separable by a vertical line
// return a pair of indices in the left and right polygons
template<typename T>
std::pair<int, int> lower_tangent_leftright(const std::vector<Point<T>> &leftpoly,
                                            const std::vector<Point<T>> &rightpoly) {
    // find leftpoly's rightmost point and rightpoly's leftmost point
    int left = std::max_element(leftpoly.begin(), leftpoly.end(), compare_lexi<T>) - leftpoly.begin();
    int right = std::min_element(rightpoly.begin(), rightpoly.end(), compare_lexi<T>) - rightpoly.begin();

    // define the vertical line and find y of its intersection with segment left_right
    double x = (double(leftpoly[left].x) + double(rightpoly[right].x))/2;
    double y = colinear_y<T>(leftpoly[left], rightpoly[right], x);

    // next points on left and right polygons
    int n_left = leftpoly.size(), n_right = rightpoly.size();
    int next_left = mod(left-1, n_left), next_right = mod(right+1, n_right);

    // intersections with vertical lines of left_nextright and nextleft_right segments
    double y_left = colinear_y<T>(leftpoly[next_left], rightpoly[right], x);
    double y_right = colinear_y<T>(leftpoly[left], rightpoly[next_right], x);

    while (y_left < y || y_right < y) { // until the y intercept is minimum
        if (y_left < y) { // advance left cw
            left = next_left;
            next_left = mod(left-1, n_left);
            y = y_left;
            y_left = colinear_y<T>(leftpoly[next_left], rightpoly[right], x);
            y_right = colinear_y<T>(leftpoly[left], rightpoly[next_right], x);
        }
        else { // advance right ccw
            right = next_right;
            next_right = mod(right+1, n_right);
            y = y_right;
            y_right = colinear_y<T>(leftpoly[left], rightpoly[next_right], x);
            y_left = colinear_y<T>(leftpoly[next_left], rightpoly[right], x);
        }
    }
    return std::make_pair(left, right);
}

// test for lower tangency
template<typename T>
bool is_lower_tangent(int left,
                      int right,
                      const std::vector<Point<T>> &leftpoly,
                      const std::vector<Point<T>> &rightpoly) {
    int next_left = mod(left+1, leftpoly.size());
    int prev_left = mod(left-1, leftpoly.size());
    int next_right = mod(right+1, rightpoly.size());
    int prev_right = mod(right-1, rightpoly.size());
    return (cross(leftpoly[left], rightpoly[right], rightpoly[next_right]) >= 0
         && cross(leftpoly[left], rightpoly[right], rightpoly[prev_right]) >= 0
         && cross(rightpoly[right], leftpoly[left], leftpoly[next_left]) <= 0
         && cross(rightpoly[right], leftpoly[left], leftpoly[prev_left]) <= 0);
}

// merge 2 polygons separable by a vertical line
template<typename T>
std::vector<Point<T>> merge_polygons_leftright(const std::vector<Point<T>> &leftpoly,
                                               const std::vector<Point<T>> &rightpoly) {
    // find upper and lower tangents
    auto upper = upper_tangent_leftright<T>(leftpoly, rightpoly);
    auto lower = lower_tangent_leftright<T>(leftpoly, rightpoly);

    std::vector<Point<T>> poly; // output

    poly.emplace_back(leftpoly[lower.first]); // add left point of lower tangent first

    // go ccw on the right poly from right point of lower tangent to
    // right point of upper tangent
    for (int i = lower.second; i != upper.second; i = mod(i+1, rightpoly.size()))
        poly.emplace_back(rightpoly[i]);

    poly.emplace_back(rightpoly[upper.second]); // add right point of upper tangent

    // go ccw on the left poly from left point of upper tangent to
    // left point of lower tangent
    for (int i = upper.first; i != lower.first; i = mod(i+1, leftpoly.size()))
        poly.emplace_back(leftpoly[i]);

    return poly;
}

// find upper and lower tangents from a point outside a convex polygon
template<typename T>
std::pair<int, int> _upper_lower_tangents(const Point<T> &p, const std::vector<Point<T>> &poly) {
    int n = poly.size();
    assert(n > 0);
    if (n == 1) return std::make_pair(0, 0);

    int upper = -1, lower = -1;
    for (int k = 0; k < n; k++) {
        int i = mod(k+1, n), j = mod(k-1, n);
        double f1 = cross(p, poly[k], poly[i]);
        double f2 = cross(p, poly[k], poly[j]);

        std::cout << "checking k = " << k << " i = " << i << " j = " << j
                  << " f1 = " << f1 << " f2 = " << f2 << std::endl;

        if (f1 == 0 && f2 == 0) { // p is on poly p = p[k]
            upper = lower = k; break;
        }
        else if (f1 >= 0 && f2 >= 0) upper = k; // found upper tangent
        else if (f1 <= 0 && f2 <= 0) lower = k; // found lower tangent
    }
    return std::make_pair(upper, lower);
}

template<typename T>
std::pair<int, int> upper_lower_tangents(const Point<T> &p, const std::vector<Point<T>> &poly) {
    assert(poly.size() > 2); // ignore degenerate polygons

    Point<T> p0 = poly[0];
    int n = poly.size(), b = 0, e = n-1, k;
    int upper = -1, lower = -1;
    while (b <= e) {
        k = (b+e)/2;
        int i = mod(k+1, n), j = mod(k-1, n);

        double f0 = cross(p, p0, poly[k]);
        double f1 = cross(p, poly[k], poly[i]);
        double f2 = cross(p, poly[k], poly[j]);

        if (f1 == 0 && f2 == 0) { // p is on poly p = p[k]
            upper = lower = k; break;
        }
        else if (f1 == 0 && cross(p, poly[i], poly[mod(i+1,n)]) == 0) {
            upper = lower = i; break; // p is on poly p = p[i]
        }
        else if (f2 == 0 && cross(p, poly[j], poly[mod(j-1,n)]) == 0) {
            upper = lower = j; break; // p is on poly p = p[j]
        }
        else if (f1 >= 0 && f2 >= 0) { // found upper tangent
            upper = k; break;
        }
        else if (f1 <= 0 && f2 <= 0) { // found lower tangent
            lower = k; break;
        }
        else {
            if ((f0 >= 0 && f1 >= 0 && f2 <= 0) || (f0 <= 0 && f1 <= 0 && f2 >= 0))
                b = i; // advance
            else if ((f0 >= 0 && f1 <= 0 && f2 >= 0) || (f0 <= 0 && f1 >= 0 && f2 <= 0))
                e = j; // backtrack
        }
    }

    if (upper != -1 && lower == -1) { // continue to find lower tangent
        b = mod(upper+1, n); e = mod(upper-1, n);
        while (distance(b, e, n) >= 0) {
            k = mid(b, e, n);
            int i = mod(k+1, n), j = mod(k-1, n);

            double f1 = cross(p, poly[k], poly[i]);
            double f2 = cross(p, poly[k], poly[j]);

            if (f1 == 0 && f2 == 0) { // p is on poly p = p[k]
                upper = lower = k; break;
            }
            else if (f1 == 0 && cross(p, poly[i], poly[mod(i+1,n)]) == 0) {
                upper = lower = i; break; // p is on poly p = p[i]
            }
            else if (f2 == 0 && cross(p, poly[j], poly[mod(j-1,n)]) == 0) {
                upper = lower = j; break; // p is on poly p = p[j]
            }
            else if (f1 <= 0 && f2 <= 0) { // found lower tangent
                lower = k; break;
            }
            else {
                if (f1 >= 0 && f2 <= 0)
                    b = i; // advance
                else if (f1 <= 0 && f2 >= 0)
                    e = j; // backtrack
            }
        }
    }
    else if (upper == -1 && lower != -1) { // continue to find upper tangent
        b = mod(lower+1, n); e = mod(lower-1, n);
        while (distance(b, e, n) >= 0) {
            k = mid(b, e, n);
            int i = mod(k+1, n), j = mod(k-1, n);

            double f1 = cross(p, poly[k], poly[i]);
            double f2 = cross(p, poly[k], poly[j]);

            if (f1 == 0 && f2 == 0) { // p is on poly p = p[k]
                upper = lower = k; break;
            }
            else if (f1 == 0 && cross(p, poly[i], poly[mod(i+1,n)]) == 0) {
                upper = lower = i; break; // p is on poly p = p[i]
            }
            else if (f2 == 0 && cross(p, poly[j], poly[mod(j-1,n)]) == 0) {
                upper = lower = j; break; // p is on poly p = p[j]
            }
            else if (f1 >= 0 && f2 >= 0) { // found upper tangent
                upper = k; break;
            }
            else {
                if (f1 <= 0 && f2 >= 0)
                    b = i; // advance
                else if (f1 >= 0 && f2 <= 0)
                    e = j; // backtrack
            }
        }
    }

    return std::make_pair(upper, lower);
}

// find all the points left of ab
template<typename T>
std::vector<Point<T>> left_points(const Point<T> &a,
                                  const Point<T> &b,
                                  const std::vector<Point<T>> &v) {
    std::vector<Point<T>> vv;
    for (const auto & p:v) {
        if (cross(a, b, p) > 0) vv.emplace_back(p);
    }
    return vv;
}

// find the point furthest from ab
template<typename T>
Point<T> furthest_points(const Point<T> &a, const Point<T> &b, const std::vector<Point<T>> &v) {
    double h = 0;
    Point<T> x = b;
    for (const auto & p:v) {
        double c = std::abs(cross(a, b, p));
        if (c > h || (c == h && cross(a, x, p) >= 0)){
            h = c; x = p;
        }
    }
    return x;
}


/*
The code below follows:
Peter Schorn, Frederick Fisher,
I.2. - Testing the Convexity of a Polygon,
Editor(s): Paul S. Heckbert,
Graphics Gems,
Academic Press,
1994,
Pages 7-15,
ISBN 9780123361561,
https://doi.org/10.1016/B978-0-12-336156-1.50011-2.
(https://www.sciencedirect.com/science/article/pii/B9780123361561500112)
*/

// given a directed line pq, determine if qr turns CW or CCW
template<typename T>
int which_side(const Point<T> &p, const Point<T> &q, const Point<T> &r) {
    double result = cross(p, q, r);
    if (result < 0) return -1; // qr turns CW
    if (result > 0) return  1; // qr turns CCW
    return 0;                  // colinear
}

// lexicographic comparison
template<typename T>
int compare(const Point<T> &p, const Point<T> &q) {
    if (p.x < q.x) return -1; // p < q
    if (p.x > q.x) return  1; // p > q
    if (p.y < q.y) return -1; // p < q
    if (p.y > q.y) return  1; // p > q
    return 0;                 // p = q
}

// get the next different point
template<typename T>
int get_different_point(const std::vector<Point<T>> &vertices, int cur) {
    int next = cur + 1;
    while (next < vertices.size()) {
        if (vertices[next] != vertices[cur]) return next;
        else next++;
    }
    return -1;
}

// test 3 consecutive points for change of direction and for orientation
template<typename T>
int check_triple(int &this_dir,
                 int &cur_dir,
                 int &dir_changes,
                 int &this_sign,
                 int &angle_sign,
                 Point<T> &first,
                 Point<T> &second,
                 Point<T> &third) {
    if ((this_dir = compare(second, third)) == -cur_dir) dir_changes++;
    cur_dir = this_dir;
    if ((this_sign = which_side(first, second, third))) {
        if (angle_sign == -this_sign) return -1; // ConvexPolygonClass::NonConvex;
        angle_sign = this_sign;
    }
    first = second; second = third;
    return 0;
}

// test convexity of a list of points
template<typename T>
ConvexPolygonClass classify_polygon(const std::vector<Point<T>> &vertices) {
    // less than 3 vertices
    if (vertices.size() < 3) return ConvexPolygonClass::ConvexDegenerate;

    // create another copy to shift to the lexicographically minimum vertex
    std::vector<Point<T>> vv(vertices.begin(), vertices.end());
    auto cmp = [] (const Point<T> &a, const Point<T> &b) {
        return (a.x < b.x || (a.x == b.x && a.y < b.y));
    };
    std::rotate(vv.begin(), std::min_element(vv.begin(), vv.end(), cmp), vv.end());

    // more than 3 vertices but all same point
    int i1 = 0, i2 = get_different_point(vv, i1);
    if (i2 < 0) return ConvexPolygonClass::ConvexDegenerate;

    // check 3 consecutive points for change of direction and orientation
    Point<T> first = vv[i1], second = vv[i2];
    int cur_dir = compare(first, second);
    int i3, this_dir, this_sign, angle_sign = 0, dir_changes = 0;

    while ((i3 = get_different_point(vv, i2)) > 0) {
        Point<T> third = vv[i3];
        if (check_triple(this_dir,
                         cur_dir,
                         dir_changes,
                         this_sign,
                         angle_sign,
                         first,
                         second,
                         third) < 0) return ConvexPolygonClass::NonConvex;
        i2 = i3;
    }

    // must check the end of list wrap back to the start properly
    // check A_n-2, A_n-1, A_0
    if (compare(second, vv[0])) { // first and last vv are different
        Point<T> third = vv[0];
        if (check_triple(this_dir,
                         cur_dir,
                         dir_changes,
                         this_sign,
                         angle_sign,
                         first,
                         second,
                         third) < 0) return ConvexPolygonClass::NonConvex;
    }

    // check A_n-1, A_0, A_1
    Point<T> third = vv[1];
    if (check_triple(this_dir,
                     cur_dir,
                     dir_changes,
                     this_sign,
                     angle_sign,
                     first,
                     second,
                     third) < 0) return ConvexPolygonClass::NonConvex;

    if (dir_changes > 2) return (angle_sign ? ConvexPolygonClass::NonConvex
                                            : ConvexPolygonClass::NonConvexDegenerate);
    if (angle_sign > 0) return ConvexPolygonClass::ConvexCCW;
    if (angle_sign < 0) return ConvexPolygonClass::ConvexCW;
    return ConvexPolygonClass::ConvexDegenerate;
}

template<typename T>
ConvexPolygonClass _classify_polygon(const std::vector<Point<T>> &vertices) {
    auto x = classify_polygon(vertices);
    switch (x) {
        case ConvexPolygonClass::NonConvex:
            std::cout << "NonConvex\n"; break;
        case ConvexPolygonClass::NonConvexDegenerate:
            std::cout << "NonConvexDegenerate\n"; break;
        case ConvexPolygonClass::ConvexCCW:
            std::cout << "ConvexCCW\n"; break;
        case ConvexPolygonClass::ConvexCW:
            std::cout << "ConvexCW\n"; break;
        case ConvexPolygonClass::ConvexDegenerate:
            std::cout << "onvexDegenerate\n"; break;
    }
    return x;
}

// check if c is on segment ab assuming abc colinear
template<typename Ta, typename Tb=Ta, typename Tc=Ta>
bool on_segment(const Point<Ta> &a, const Point<Tb> &b, const Point<Tc> &c) {
    if (std::min(a.x, b.x) <= c.x && std::max(a.x, b.x) >= c.x &&
        std::min(a.y, b.y) <= c.y && std::max(a.y, b.y) >= c.y)
        return true;
    else return false;
}

template<typename Tab, typename Tc=Tab>
bool on_segment(const Segment<Tab> &ab, const Point<Tc> &c) {
    return on_segment(ab.p1, ab.p2, c);
}

// check if segments p1p2 p3p4 intersect
template<typename T1, typename T2=T1, typename T3=T1, typename T4=T1>
bool segments_intersect(const Point<T1> &p1,
                        const Point<T2> &p2,
                        const Point<T3> &p3,
                        const Point<T4> &p4) {
    double d1 = cross(p3, p4, p1), d2 = cross(p3, p4, p2);
    double d3 = cross(p1, p2, p3), d4 = cross(p1, p2, p4);
    if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
        ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0)))
        return true; // intersect
    else if (d1 == 0 && on_segment(p3, p4, p1)) return true; // p1 is on p3p4
    else if (d2 == 0 && on_segment(p3, p4, p2)) return true; // p2 is on p3p4
    else if (d3 == 0 && on_segment(p1, p2, p3)) return true; // p3 is on p1p2
    else if (d4 == 0 && on_segment(p1, p2, p4)) return true; // p4 is on p1p2
    else return false;
}

template<typename Tab, typename Tcd=Tab>
bool segments_intersect(const Segment<Tab> &ab, const Segment<Tcd> &cd) {
    return segments_intersect(ab.p1, ab.p2, cd.p1, cd.p2);
}

// compute intersection point segments p1p2 p3p4
template<typename T1, typename T2=T1, typename T3=T1, typename T4=T1>
SegmentIntersectionType segments_intersection(const Point<T1> &p1,
                                              const Point<T2> &p2,
                                              const Point<T3> &p3,
                                              const Point<T4> &p4,
                                              Point2d *p = nullptr) {
    double d1 = cross(p3, p4, p1), d2 = cross(p3, p4, p2);
    double d3 = cross(p1, p2, p3), d4 = cross(p1, p2, p4);
    if (((d1 > 0 && d2 < 0) || (d1 < 0 && d2 > 0)) &&
        ((d3 > 0 && d4 < 0) || (d3 < 0 && d4 > 0))) { // crossing
        if (p) {
            double x12 = p1.x - p2.x, x34 = p3.x - p4.x, x42 = p4.x - p2.x;
            double y12 = p1.y - p2.y, y34 = p3.y - p4.y, y42 = p4.y - p2.y;
            double t = (x42*y34 - y42*x34)/(x12*y34 - y12*x34);
            p->x = p2.x + t*x12; p->y = p2.y + t*y12;
        }
        return SegmentIntersectionType::Crossing;
    }
    else if (d1 == 0 && on_segment(p3, p4, p1)) { // p1 is on p3p4
        if (p) { p->x = p1.x; p->y = p1.y;}
        return (d2 == 0)? SegmentIntersectionType::Overlapping:
                          SegmentIntersectionType::EndPointTouch;
    }
    else if (d2 == 0 && on_segment(p3, p4, p2)) { // p2 is on p3p4
        if (p) { p->x = p2.x; p->y = p2.y;}
        return (d1 == 0)? SegmentIntersectionType::Overlapping:
                          SegmentIntersectionType::EndPointTouch;
    }
    else if (d3 == 0 && on_segment(p1, p2, p3)) { // p3 is on p1p2
        if (p) { p->x = p3.x; p->y = p3.y;}
        return (d4 == 0)? SegmentIntersectionType::Overlapping:
                          SegmentIntersectionType::EndPointTouch;
    }
    else if (d4 == 0 && on_segment(p1, p2, p4)) { // p4 is on p1p2
        if (p) { p->x = p4.x; p->y = p4.y;}
        return (d3 == 0)? SegmentIntersectionType::Overlapping:
                          SegmentIntersectionType::EndPointTouch;
    }
    else return SegmentIntersectionType::None;
}

template<typename Tab, typename Tcd=Tab>
SegmentIntersectionType segments_intersection(const Segment<Tab> &ab,
                                              const Segment<Tcd> &cd,
                                              Point2d *p = nullptr) {
    return segments_intersection(ab.p1, ab.p2, cd.p1, cd.p2, p);
}

// compute area
template<typename T>
double area(const std::vector<Point<T>> &points) {
    size_t n = points.size(), n1 = n-1;
    double a = 0;
    for (size_t i = 0; i < n1; i++) {
        a += (double(points[i].x) + double(points[i+1].x))*
             (double(points[i+1].y) - double(points[i].y));
    }
    a += (double(points[n1].x) + double(points[0].x))*
         (double(points[0].y) - double(points[n1].y));
    return a/2;
}

// check direction
template<typename T>
bool is_ccw(const std::vector<Point<T>> &points) {
    return area(points) >= 0;
}

#endif
