#ifndef TRIANGULATION_H
#define TRIANGULATION_H

#include "geometry.h"
#include "geomutil.h"
#include "dcel.h"
#include "bst.h"
#include <algorithm>

enum class VertexType { StartVertex,
                        EndVertex,
                        RegularVertex,
                        SplitVertex,
                        MergeVertex};

template<typename T>
struct Vertex {
    Vertex(): x(0), y(0), type(VertexType::StartVertex), id(-1) {}
    Vertex(T a, T b, VertexType t, int i): x(a), y(b), type(t), id(i) {}
    Vertex(const Vertex<T> &other): x(other.x), y(other.y), type(other.type), id(other.id){}
    Vertex<T>& operator=(const Vertex<T> &other) {
        x = other.x; y = other.y; type = other.type; id = other.id;
        return *this;
    }
    Vertex(Vertex<T> &&other): x(other.x), y(other.y), type(other.type), id(other.id){}
    ~Vertex() {}

    T x, y;
    VertexType type;
    int id;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const Vertex<T> &p) {
    os << "(" << p.x << ", " << p.y << ")";
    return os;
}

template<typename T>
struct Edge {
    Edge(): x1(0), y1(0), x2(0), y2(0), helper(nullptr) {}
    Edge(T a1, T b1, T a2, T b2): x1(a1), y1(b1), x2(a2), y2(b2), helper(nullptr) {}

    T x1, y1, x2, y2;
    Vertex<T> *helper = nullptr;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const Edge<T> &e) {
    os << "[(" << e.x1 << ", " << e.y1 << "), " << e.x2 << ", " << e.y2 << ")]";
    return os;
}

typedef Vertex<int> Vertex2i;
typedef Edge<int> Edge2i;

// find x such that 3 points (x, y), (xi, yi) are colinear
// ie intersection of horizontal line at y and segment x1y1x2y2
// assume y1 != y2
template<typename T>
inline double colinear_x(const Edge<T> &e, double y) {
    return colinear_x(double(e.x1), double(e.y1), double(e.x2), double(e.y2), y);
}

// classify vertices into different types and sort
// input is a list of points in ccw
template<typename T>
std::vector<Vertex<T>> process_vertices(const std::vector<Point<T>> &points) {
    std::vector<Vertex<T>> v;
    int n = points.size();

    for (int i = 0; i < n; i++) {
        int prev = mod(i-1, n), next = mod(i+1, n);
        VertexType t;
        auto c = cross(points[prev], points[i], points[next]);

        if (is_higher(points[i], points[prev]) &&
            is_higher(points[i], points[next])) {
            if (c > 0) t = VertexType::StartVertex;
            else       t = VertexType::SplitVertex;
        }
        else if (!is_higher(points[i], points[prev]) &&
                 !is_higher(points[i], points[next])) {
            if (c > 0) t = VertexType::EndVertex;
            else       t = VertexType::MergeVertex;
        }
        else           t = VertexType::RegularVertex;

        v.emplace_back(points[i].x, points[i].y, t, i);
    }

    std::sort(v.begin(), v.end(), is_higher<Vertex<T>>);
    return v;
}

// create list of edges
template<typename T>
std::vector<Edge<T>> process_edges(const std::vector<Point<T>> &points) {
    std::vector<Edge<T>> ve;
    int n = points.size();
    for (int i = 0; i < n; i++) {
        int j = mod(i+1, n);
        ve.emplace_back(points[i].x, points[i].y, points[j].x, points[j].y);
    }
    return ve;
}

template<typename T>
void monotone_decompose(const std::vector<Point<T>> &points, DCEL<T> &dcel) {
    // classify points into different types and sort nin y order
    std::vector<Vertex<T>> vertices = process_vertices(points);
    std::vector<Edge<T>> edges = process_edges(points);
    BSTNode<Edge<T>> *edge_treehead = nullptr; // to store comparable edges

    // sweep vertices in decreasing y order
    int nv = points.size(); // also number of events

    for (Vertex<T> &vertex: vertices) {
        // compare ab and cd by x coordinates of intersections with horizontal line at y
        auto cmp = [y = double(vertex.y)] (const Edge<T> &ab, const Edge<T> &cd) {
            double x_ab = (ab.y1 == ab.y2)? std::min(ab.x1, ab.x2): colinear_x(ab, y);
            double x_cd = (cd.y1 == cd.y2)? std::min(cd.x1, cd.x2): colinear_x(cd, y);
            return x_ab - x_cd;
        };

        // process vertex depending on type
        if (vertex.type == VertexType::StartVertex) {
            edges[vertex.id].helper = &vertex; // set helper
            insert(edge_treehead, &edges[vertex.id], cmp); // insert edge to bst
        }
        else if (vertex.type == VertexType::EndVertex) {
            int prev = mod(vertex.id-1, nv);
            if (edges[prev].helper->type == VertexType::MergeVertex) {
                // insert diagonal from v to helper
                DCELFace<T> *face = find_face(dcel, edges[prev].x1, edges[prev].y1,
                                                    edges[prev].x2, edges[prev].y2);
                split_face(dcel, face, vertex.x, vertex.y, edges[prev].helper->x, edges[prev].helper->y);
            }
            del(edge_treehead, &edges[prev], cmp); // delete prev edge
        }
        else if (vertex.type == VertexType::SplitVertex) {
            // find edge directly left of v using edge at v as proxy
            Edge<T> *lefte = floor(edge_treehead, &edges[vertex.id], cmp)->data;
            // insert diagonal from v to helper of lefte
            DCELFace<T> *face = find_face(dcel, lefte->x1, lefte->y1, lefte->x2, lefte->y2);
            split_face(dcel, face, vertex.x, vertex.y, lefte->helper->x, lefte->helper->y);
            lefte->helper = &vertex; // set helper
            edges[vertex.id].helper = &vertex; // set helper
            insert(edge_treehead, &edges[vertex.id], cmp); // insert edge to bst
        }
        else if (vertex.type == VertexType::MergeVertex) {
            int prev = mod(vertex.id-1, nv);
            if (edges[prev].helper->type == VertexType::MergeVertex) {
                // insert diagonal from v to helper
                DCELFace<T> *face = find_face(dcel, edges[prev].x1, edges[prev].y1,
                                                    edges[prev].x2, edges[prev].y2);
                split_face(dcel, face, vertex.x, vertex.y, edges[prev].helper->x, edges[prev].helper->y);
            }
            del(edge_treehead, &edges[prev], cmp); // delete prev edge
            // find edge directly left of v using edge at v as proxy
            Edge<T> *lefte = floor(edge_treehead, &edges[vertex.id], cmp)->data;
            if (lefte->helper->type == VertexType::MergeVertex) {
                // insert diagonal from v to helper of lefte
                DCELFace<T> *face = find_face(dcel, lefte->x1, lefte->y1, lefte->x2, lefte->y2);
                split_face(dcel, face, vertex.x, vertex.y, lefte->helper->x, lefte->helper->y);
            }
            lefte->helper = &vertex; // set helper
        }
        else if (vertex.type == VertexType::RegularVertex) {
            int prev = mod(vertex.id-1, nv);
            if (points[prev].y > vertex.y) { // polygon interior is on the right of v
                if (edges[prev].helper->type == VertexType::MergeVertex) {
                    // insert diagonal from v to helper
                    DCELFace<T> *face = find_face(dcel, edges[prev].x1, edges[prev].y1,
                                                        edges[prev].x2, edges[prev].y2);
                    split_face(dcel, face, vertex.x, vertex.y, edges[prev].helper->x, edges[prev].helper->y);
                }
                del(edge_treehead, &edges[prev], cmp); // delete prev edge
                edges[vertex.id].helper = &vertex; // set helper
                insert(edge_treehead, &edges[vertex.id], cmp); // insert edge to bst
            }
            else { // polygon interior is on the left of
                // find edge directly left of v using edge at v as proxy
                Edge<T> *lefte = floor(edge_treehead, &edges[vertex.id], cmp)->data;
                if (lefte->helper->type == VertexType::MergeVertex) {
                    // insert diagonal from v to helper of lefte
                    DCELFace<T> *face = find_face(dcel, lefte->x1, lefte->y1, lefte->x2, lefte->y2);
                    split_face(dcel, face, vertex.x, vertex.y, lefte->helper->x, lefte->helper->y);
                }
                lefte->helper = &vertex; // set helper
            }
        }
    }
    return;
}

#endif
