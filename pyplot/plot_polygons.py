#!/usr/local/bin/python3.12

import matplotlib.pyplot as plt
import argparse

from geomplot import *

parser = argparse.ArgumentParser()

parser.add_argument("-f", "--filenames", nargs="+", help="Files to plot", required=True)
parser.add_argument("-c", "--color", nargs="*", help="Color")
parser.add_argument("-label", "--labeling", action="store_true", help="If want to label points")

args = parser.parse_args()
fig, ax = plt.subplots()
n = len(args.filenames)

for i in range(n):
    color = 'k' if args.color is None else args.color[i]
    params = PlotParams(args.filenames[i], color, args.labeling)
    plot_polygons(ax, params)

plt.show()
