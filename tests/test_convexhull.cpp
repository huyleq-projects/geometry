#include "convexhull.h"
#include "geomutil.h"

#include <gtest/gtest.h>

#include <iostream>
#include <cstdlib>
#include <chrono>
#include <random>
#include <limits>

TEST(TestConvexHull, Jarvis) {
    // compute convex hull using jarvis
    std::vector<Point2i> vp = random_test_points();
    std::vector<Point2i> hull = convexhull_jarvis(vp);

    // check
    bool check = (classify_polygon<int>(hull) == ConvexPolygonClass::ConvexCCW);
    if (!check) {
        write_to_file("jarvisinput.txt", vp.begin(), vp.end());
        write_to_file("jarvishull.txt", hull.begin(), hull.end());
    }
    EXPECT_TRUE(check);
}

TEST(TestConvexHull, Graham) {
    // make a random list of points
    std::vector<Point2i> vp = random_test_points();

    // compute convex hull using graham
    std::vector<Point2i> hull = convexhull_graham<int>(vp);

    // check
    bool check = (classify_polygon<int>(hull) == ConvexPolygonClass::ConvexCCW);
    if (!check) {
        write_to_file("grahaminput.txt", vp.begin(), vp.end());
        write_to_file("grahamhull.txt", hull.begin(), hull.end());
    }
    EXPECT_TRUE(check);
}

TEST(TestConvexHull, DivideConquer) {
    // make a random list of points
    std::vector<Point2i> vp = random_test_points();

    // compute convex hull using divide and conquer
    std::vector<Point2i> hull = convexhull_divideconquer<int>(vp);

    // check
    bool check = (classify_polygon<int>(hull) == ConvexPolygonClass::ConvexCCW);
    if (!check) {
        write_to_file("divideconquerinput.txt", vp.begin(), vp.end());
        write_to_file("divideconquerhull.txt", hull.begin(), hull.end());
    }
    EXPECT_TRUE(check);
}

TEST(TestConvexHull, Chan) {
    // make a random list of points
    std::vector<Point2i> vp = random_test_points();

    // compute convex hull using Chan's algo
    std::vector<Point2i> hull = convexhull_chan(vp);

    // check
    bool check = (classify_polygon<int>(hull) == ConvexPolygonClass::ConvexCCW);
    if (!check) {
        write_to_file("chaninput.txt", vp.begin(), vp.end());
        write_to_file("chanhull.txt", hull.begin(), hull.end());
    }
    EXPECT_TRUE(check);
}

TEST(TestConvexHull, UpperLowerTangents) {
    // compute convex hull using jarvis
    std::vector<Point2i> vp = random_test_points();
    std::vector<Point2i> hull = convexhull_jarvis(vp);

    // bbox and xy limits
    auto b = bbox(hull);
    std::cout << "ll: " << b.first << " ur: " << b.second << std::endl;
    int xymin = std::min(b.first.x, b.first.y) - 1;
    int xymax = std::max(b.second.x, b.second.y) + 1;
    int intmax = std::numeric_limits<int>::max();
    int intmin = std::numeric_limits<int>::min();

    // distributions of xy coordinates outside of bbox
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distributionx(xymax, intmax);
    std::uniform_int_distribution<int> distributiony(intmin, xymin);
    std::uniform_int_distribution<int> distribution(intmin, intmax);

    // pick random points outside of bbox
    std::vector<Point2i> v;
    v.emplace_back(distributionx(generator), distribution(generator));
    v.emplace_back(distributionx(generator), distribution(generator));
    v.emplace_back(distribution(generator), distributiony(generator));
    v.emplace_back(distribution(generator), distributiony(generator));

    // check tangents
    for (auto p: v) {
        std::cout << "checking tangents from " << p << std::endl;
        auto a = upper_lower_tangents(p, hull);
        EXPECT_TRUE(a.first != -1 && a.second != -1);
    }

    // special cases
    std::vector<Point2i> poly;
    poly.emplace_back(1, 0); poly.emplace_back(3, 0);
    poly.emplace_back(3, 3); poly.emplace_back(1, 1);

    std::cout << "case point on 2 edges\n";
    Point2i x(0, 0);
    auto tx = upper_lower_tangents(x, poly);
    EXPECT_TRUE((tx.first == 0 || tx.first == 1) && (tx.second == 2 || tx.second == 3));

    std::cout << "case point on poly\n";
    Point2i y(3, 3);
    auto ty = upper_lower_tangents(y, poly);
    std::cout << "upper = " << ty.first << " lower = " << ty.second << std::endl;
    EXPECT_TRUE(ty.first == 2 && ty.second == 2);
}

TEST(TestConvexHull, Quickhull) {
    // make a random list of points
    std::vector<Point2i> vp = random_test_points();

    // compute convex hull using quickhull
    std::vector<Point2i> hull = convexhull_quickhull<int>(vp);

    // check
    bool check = (classify_polygon<int>(hull) == ConvexPolygonClass::ConvexCCW);
    if (!check) {
        write_to_file("quickinput.txt", vp.begin(), vp.end());
        write_to_file("quickhull.txt", hull.begin(), hull.end());
    }
    EXPECT_TRUE(check);
}

