#ifndef DCEL_H
#define DCEL_H

#include "geometry.h"
#include <iostream>
#include <vector>
#include <limits>

// doubly connected edge list
template<typename T>
struct DCELHalfEdge;

template<typename T>
struct DCELVertex {
    DCELVertex(): x(0), y(0), incident_edge(nullptr) {}
    DCELVertex(T a, T b): x(a), y(b), incident_edge(nullptr) {}

    DCELVertex(const DCELVertex<T> &other): x(other.x),
                                            y(other.y),
                                            incident_edge(other.incident_edge) {}

    DCELVertex<T>& operator=(const DCELVertex<T> &other) {
        x = other.x; y = other.y; incident_edge = other.incident_edge;
        return *this;
    }

    DCELVertex(DCELVertex<T> &&other): x(other.x),
                                       y(other.y),
                                       incident_edge(other.incident_edge) {}

    ~DCELVertex() {}

    T x, y;
    DCELHalfEdge<T> *incident_edge;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const DCELVertex<T> &p) {
    os << "(" << p.x << ", " << p.y << ")";
    return os;
}

template<typename T>
struct DCELFace {
    DCELFace(): outer_edge(nullptr) {}
    DCELFace(const DCELFace<T> &other): outer_edge(other.outer_edge),
                                        inner_edges(other.inner_edges) {}

    DCELFace<T>& operator=(const DCELFace<T> &other) {
        outer_edge = other.outer_edge;
        inner_edges = other.inner_edges;
        return *this;
    }

    DCELFace(DCELFace<T> &&other): outer_edge(other.outer_edge),
                                   inner_edges(other.inner_edges) {}

    ~DCELFace() {}

    DCELHalfEdge<T> *outer_edge;
    std::vector<DCELHalfEdge<T>*> inner_edges;
};

template<typename T>
struct DCELHalfEdge {
    DCELHalfEdge(): origin(nullptr),
                    twin(nullptr),
                    incident_face(nullptr),
                    prev(nullptr),
                    next(nullptr) {}

    DCELHalfEdge(const DCELHalfEdge<T> &other): origin(other.origin),
                                                twin(other.twin),
                                                incident_face(other.incident_face),
                                                prev(other.prev),
                                                next(other.next) {}

    DCELHalfEdge<T>& operator=(const DCELHalfEdge<T> &other) {
        origin = other.origin;
        twin = other.twin;
        incident_face = other.incident_face;
        prev = other.prev;
        next = other.next;
        return *this;
    }

    DCELHalfEdge(DCELHalfEdge<T> &&other): origin(other.origin),
                                           twin(other.twin),
                                           incident_face(other.incident_face),
                                           prev(other.prev),
                                           next(other.next) {}

    ~DCELHalfEdge() {}

    DCELVertex<T> *origin;
    DCELHalfEdge<T> *twin;
    DCELFace<T> *incident_face;
    DCELHalfEdge<T> *prev;
    DCELHalfEdge<T> *next;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const DCELHalfEdge<T> &e) {
    os << "[" << (*e.origin) << "-> " << (*e.twin->origin) << "]";
    return os;
}

template<typename T>
std::ostream &operator<<(std::ostream &os, DCELFace<T> &f) {
    os << "{";
    if (f.outer_edge) {
        os << "Outer: ";
        DCELHalfEdge<T> *e = f.outer_edge;
        while (e->next != f.outer_edge) {
            os << (*e) << " ";
            e = e->next;
        }
        os << (*e);
    }
    for (auto inner_edge: f.inner_edges) {
        if (inner_edge) {
            os << "Inner: ";
            DCELHalfEdge<T> *e = inner_edge;
            while (e->next != inner_edge) {
                os << (*e) << " ";
                e = e->next;
            }
            os << (*e);
        }
    }
    os << "}";
    return os;
}

template<typename T>
struct DCEL {
    DCEL() {}
    DCEL(const std::vector<Point<T>> &points);
    DCEL(const DCEL<T> &other): vertices(other.vertices),
                                faces(other.faces),
                                half_edges(other.half_edges) {}
    DCEL& operator=(const DCEL<T> &other) {
        vertices = other.vertices;
        faces = other.faces;
        half_edges = other.half_edges;
        return *this;
    }
    DCEL(DCEL<T> &&other): vertices(other.vertices),
                           faces(other.faces),
                           half_edges(other.half_edges) {}
    void clean() {
        for (auto &x: vertices) delete x;
        for (auto &x: faces) delete x;
        for (auto &x: half_edges) delete x;
    }

    void write_to_file(const std::string &filename) const;

    ~DCEL() {}

    std::vector<DCELVertex<T>*> vertices;
    std::vector<DCELFace<T>*> faces;
    std::vector<DCELHalfEdge<T>*> half_edges;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const DCEL<T> &dcel) {
    os << "Vertices: ";
    for (auto x: dcel.vertices) os << (*x) << " ";
    os << std::endl;

    os << "Halfedges: ";
    for (auto x: dcel.half_edges) os << (*x) << " ";
    os << std::endl;

    os << "Faces: ";
    for (auto x: dcel.faces) os << (*x) << " ";
    return os;
}

typedef DCELVertex<int> DCELVertex2i;
typedef DCELFace<int> DCELFace2i;
typedef DCELHalfEdge<int> DCELHalfEdge2i;
typedef DCEL<int> DCEL2i;

template<typename T>
void DCEL<T>::write_to_file(const std::string &filename) const {
    std::ofstream ofs(filename);
    if (ofs.is_open()) {
        for (auto f: faces) {
            if (f->outer_edge) {
                DCELHalfEdge<T> *e = f->outer_edge;
                while (e->next != f->outer_edge) {
                    ofs << (*e->origin) << " ";
                    e = e->next;
                }
                ofs << (*e->origin) << std::endl;
            }
            for (auto inner_edge: f->inner_edges) {
                if (inner_edge) {
                    DCELHalfEdge<T> *e = inner_edge;
                    while (e->next != inner_edge) {
                        ofs << (*e->origin) << " ";
                        e = e->next;
                    }
                    ofs << (*e->origin) << std::endl;
                }
            }
        }
        ofs.close();
    }
    else perror("write_to_file error");
    return;

}

// construct from a simple ccw polygon with half edges numbered as follows
//      5
//    --<--
//   |  2  |
//  4|3   1|6
//   |  0  |
//    -->--
//      7
template<typename T>
DCEL<T>::DCEL(const std::vector<Point<T>> &points) {
    size_t n = points.size(), m = 2*n, m1 = m-1;
    vertices.resize(n); half_edges.resize(m); faces.resize(2);
    for (size_t i = 0; i < n; i++) vertices[i] = new DCELVertex<T>;
    for (size_t i = 0; i < m; i++) half_edges[i] = new DCELHalfEdge<T>;
    for (size_t i = 0; i < 2; i++) faces[i] = new DCELFace<T>;

    for (size_t i = 0; i < n; i++) {
        vertices[i]->x = points[i].x; vertices[i]->y = points[i].y;
        vertices[i]->incident_edge = half_edges[i];

        half_edges[i]->twin = half_edges[m1-i];
        half_edges[m1-i]->twin = half_edges[i];

        half_edges[i]->incident_face = faces[1];
        half_edges[m1-i]->incident_face = faces[0];

        int j = mod(i+1, n); // next edge
        half_edges[i]->next = half_edges[j];
        half_edges[i+n]->next = half_edges[j+n];

        j = mod(i-1, n); // prev edge
        half_edges[i]->prev = half_edges[j];
        half_edges[i+n]->prev = half_edges[j+n];

        half_edges[i]->origin = vertices[i];
        half_edges[m1-j]->origin = vertices[i];
    }

    faces[0]->outer_edge = nullptr; // unbounded face outside polygon
    faces[0]->inner_edges.push_back(half_edges[m1]);
    faces[1]->outer_edge = half_edges[0]; // polygon face
}

// find the face that contains edge from (x1, y1) to (x2, y2)
template<typename T>
DCELFace<T>* find_face(DCEL<T> &dcel, T x1, T y1, T x2, T y2) {
    size_t nf = dcel.faces.size();
    DCELFace<T> *face = nullptr;
    for (size_t i = 0; i < nf && !face; i++) {
        DCELFace<T> *f = dcel.faces[i];
        if (f->outer_edge) {
            DCELHalfEdge<T> *e = f->outer_edge;
            while (e->next != f->outer_edge && !face) {
                if (e->origin->x == x1 && e->twin->origin->x == x2 &&
                    e->origin->y == y1 && e->twin->origin->y == y2)
                    face = f;
                e = e->next;
            }
            if (e->origin->x == x1 && e->twin->origin->x == x2 &&
                e->origin->y == y1 && e->twin->origin->y == y2)
                face = f;
        }
    }
    return face;
}

template<typename T>
DCELFace<T>* find_face(DCEL<T> &dcel, const Point<T> &p1, const Point<T> &p2) {
    return find_face(dcel, p1.x, p1.y, p2.x, p2.y);
}

// split face by an edge from [(x1, y1), (x2, y2)] assuming the last face has no hole
template<typename T>
bool split_face(DCEL<T> &dcel, DCELFace<T> *f, T x1, T y1, T x2, T y2) {
    // loop over all outer edges of face to find edges coming out of (x1, y1) and (x2, y2)
    DCELHalfEdge<T> *oe = f->outer_edge;
    if (!oe) return false; // face does not have outer boundary

    DCELHalfEdge<T> *e = oe, *e1 = nullptr, *e2 = nullptr;
    while (e->next != oe) {
        if      (e->origin->x == x1 && e->origin->y == y1) e1 = e;
        else if (e->origin->x == x2 && e->origin->y == y2) e2 = e;
        e = e->next;
    }
    if      (e->origin->x == x1 && e->origin->y == y1) e1 = e;
    else if (e->origin->x == x2 && e->origin->y == y2) e2 = e;

    if (!e1 || !e2) return false; // no edge from v1 or v2

    // create 2 new half edges
    DCELHalfEdge<T> *e12 = new DCELHalfEdge<T>; // new edge from v1 to v2
    DCELHalfEdge<T> *e21 = new DCELHalfEdge<T>; // new edge from v2 to v1
    dcel.half_edges.push_back(e12);
    dcel.half_edges.push_back(e21);

    e12->origin = e1->origin;
    e21->origin = e2->origin;
    e12->twin = e21;
    e21->twin = e12;
    e12->next = e2;
    e21->next = e1;
    e12->prev = e1->prev;
    e21->prev = e2->prev;

    e1->prev->next = e12;
    e1->prev = e21;
    e2->prev->next = e21;
    e2->prev = e12;

    // create a new face
    dcel.faces.push_back(new DCELFace<T>);
    DCELFace<T> *f12 = f;
    DCELFace<T> *f21 = dcel.faces.back();
    f12->outer_edge = e12;
    f21->outer_edge = e21;

    // now reset incident_face of all involved half edges
    e = e12;
    while (e->next != e12) {
        e->incident_face = f12;
        e = e->next;
    }
    e->incident_face = f12;

    e = e21;
    while (e->next != e21) {
        e->incident_face = f21;
        e = e->next;
    }
    e->incident_face = f21;

    return true;
}

template<typename T>
bool split_face(DCEL<T> &dcel, DCELFace<T> *f, const Point<T> &p1, const Point<T> &p2) {
    return split_face<T>(dcel, f, p1.x, p1.y, p2.x, p2.y);
}

#endif
