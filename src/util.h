#ifndef UTIL_H
#define UTIL_H

#include <cmath>
#include <iostream>
#include <fstream>
#include <string>

// is practically 0 in machine precision
template<typename T>
inline bool is_zero_single(T x) {
    return std::abs(x) <= 1e-5;
}

template<typename T>
inline bool is_zero_double(T x) {
    return std::abs(x) <= 1e-15;
}

// circular remainder
inline int mod(int x, int n) {
    int r = x%n;
    return (r < 0? (r+n): r);
}

// circular mid point
inline int mid(int b, int e, int n) {
    if (b <= e) return (b+e)/2;
    else return mod((b+e+n)/2, n);
}

// circular distance
inline int distance(int b, int e, int n) {
    if (b <= e) return (e-b);
    else return (n-(b-e));
}

// dot product
inline double dot(double x1, double y1, double x2, double y2) {
    return (x1*x2 + y1*y2);
}

// 2x2 determinant | x1 y1 |
//                 | x2 y1 |
inline double det(double x1, double y1, double x2, double y2) {
    return (x1*y2 - x2*y1);
}

// find y such that 3 points (x, y), (xi, yi) are colinear
// ie intersection of vertical line at x and segment x1y1x2y2
// assume x1 != x2
inline double colinear_y(double x1, double y1, double x2, double y2, double x) {
    return y1 + (y2-y1)*(x-x1)/(x2-x1);
}

// find x such that 3 points (x, y), (xi, yi) are colinear
// ie intersection of horizontal line at y and segment x1y1x2y2
// assume y1 != y2
inline double colinear_x(double x1, double y1, double x2, double y2, double y) {
    return x1 + (x2-x1)*(y-y1)/(y2-y1);
}

// print an array one item per line
template<class ForwardIterator>
void print(ForwardIterator first, ForwardIterator last) {
    for (ForwardIterator it = first; it != last; it++)
        std::cout << *it << std::endl;
    return;
}

// write array to file one item per line
template<class ForwardIterator>
void write_to_file(const std::string &filename,
                   ForwardIterator first,
                   ForwardIterator last,
                   bool newline=true) {
    std::ofstream ofs(filename);
    if (ofs.is_open()) {
        for (ForwardIterator it = first; it != last; it++) {
            if (newline) ofs << *it << std::endl;
            else         ofs << *it << " ";
        }
        ofs.close();
    }
    else perror("write_to_file error");
    return;
}

#endif
