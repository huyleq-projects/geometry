#ifndef GEOMETRY_H
#define GEOMETRY_H

#include "util.h"

#include <cassert>
#include <iostream>
#include <vector>
#include <cmath>

template<typename T>
struct Point {
    Point(): x(0), y(0) {}
    Point(T x0, T y0): x(x0), y(y0) {}
    template<typename T1> Point(const Point<T1> &p): x(p.x), y(p.y) {}
    template<typename T1> Point &operator=(const Point<T1> &p) {
        x = p.x; y = p.y;
        return *this;
    }
    template<typename T1> bool operator==(const Point<T1> &p) const {
        return (x == p.x && y == p.y);
    }
    template<typename T1> bool operator!=(const Point<T1> &p) const {
        return !(*this == p);
    }

    T x, y;
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const Point<T> &p) {
    os << "(" << p.x << ", " << p.y << ")";
    return os;
}

template<typename T>
struct Segment {
    Segment() {}
    Segment(T x1, T y1, T x2, T y2): p1(x1, y1), p2(x2, y2) {}
    Segment(const Point<T> &a, const Point<T> &b): p1(a), p2(b) {}
    Segment(const Segment &v): p1(v.p1), p2(v.p2) {}
    Segment &operator=(const Segment &v) {
        p1 = v.p1; p2 = v.p2;
        return *this;
    }

    Point<T> p1, p2;
};

template<typename T>
std::ostream & operator<<(std::ostream &os, const Segment<T> &v) {
    os << "[" << v.p1 << v.p2 << "]";
    return os;
}

template<typename T>
struct Polygon {
    Polygon() {}
    Polygon(const std::vector<Point<T>> &vp) {
        assert(vp.size() > 2);
        for (const Point<T> &p: vp) vertices.emplace_back(p);
    }

    std::vector<Point<T>> vertices;
};

template<typename T>
std::ostream & operator<<(std::ostream &os, const Polygon<T> &poly) {
    os << "(";
    for(const Point<T> &p: poly.vertices) os << p;
    os << ")";
    return os;
}

typedef Point<int> Point2i;
typedef Point<double> Point2d;
typedef Segment<int> Segment2i;
typedef Segment<double> Segment2d;

enum class Direction { CCW, CW};

enum class ConvexPolygonClass { NonConvex,
                                NonConvexDegenerate,
                                ConvexDegenerate,
                                ConvexCCW,
                                ConvexCW};

enum class SegmentIntersectionType { None,
                                     Overlapping,
                                     EndPointTouch,
                                     Crossing};


#endif
