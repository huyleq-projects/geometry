#include "staircase.h"
#include "geomutil.h"
#include <gtest/gtest.h>
#include <iostream>

TEST(TestStaircase, RandomInput) {
    std::vector<Point2i> v = random_test_points();
    EXPECT_TRUE(valid_staircase(staircase(v), v));
}

