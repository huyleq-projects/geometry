#include "geomutil.h"
#include "triangulation.h"

#include <gtest/gtest.h>
#include <iostream>

TEST(TestTriangulation, ProcessVertices) {
    std::vector<Point2i> points;
    points.emplace_back(0, 0); points.emplace_back(3, -1);
    points.emplace_back(4, 1); points.emplace_back(2, 3);
    points.emplace_back(1, 8); points.emplace_back(-1, 7);
    points.emplace_back(-2, 8); points.emplace_back(-5, 7);
    points.emplace_back(-3, 5); points.emplace_back(-4, 2);
    points.emplace_back(-5, 4); points.emplace_back(-6, 0);
    points.emplace_back(-3, -3); points.emplace_back(0, -2);
    points.emplace_back(1, -3);
    write_to_file("points.txt", points.begin(), points.end());

    std::vector<Vertex2i> v = process_vertices(points);

    EXPECT_TRUE(v[0].x == -2 && v[0].y == 8 && v[0].type == VertexType::StartVertex);
    EXPECT_TRUE(v[1].x == 1  && v[1].y == 8 && v[1].type == VertexType::StartVertex);
    EXPECT_TRUE(v[2].x == -5 && v[2].y == 7 && v[2].type == VertexType::RegularVertex);
    EXPECT_TRUE(v[3].x == -1 && v[3].y == 7 && v[3].type == VertexType::MergeVertex);
    EXPECT_TRUE(v[4].x == -3 && v[4].y == 5 && v[4].type == VertexType::RegularVertex);
    EXPECT_TRUE(v[5].x == -5 && v[5].y == 4 && v[5].type == VertexType::StartVertex);
    EXPECT_TRUE(v[6].x == 2  && v[6].y == 3 && v[6].type == VertexType::RegularVertex);
    EXPECT_TRUE(v[7].x == -4 && v[7].y == 2 && v[7].type == VertexType::MergeVertex);
    EXPECT_TRUE(v[8].x == 4  && v[8].y == 1 && v[8].type == VertexType::RegularVertex);
    EXPECT_TRUE(v[9].x == -6 && v[9].y == 0 && v[9].type == VertexType::RegularVertex);
    EXPECT_TRUE(v[10].x == 0  && v[10].y == 0  && v[10].type == VertexType::SplitVertex);
    EXPECT_TRUE(v[11].x == 3  && v[11].y == -1 && v[11].type == VertexType::EndVertex);
    EXPECT_TRUE(v[12].x == 0  && v[12].y == -2 && v[12].type == VertexType::SplitVertex);
    EXPECT_TRUE(v[13].x == -3 && v[13].y == -3 && v[13].type == VertexType::EndVertex);
    EXPECT_TRUE(v[14].x == 1  && v[14].y == -3 && v[14].type == VertexType::EndVertex);
}

TEST(TestTriangulation, MonotoneDecompose1) {
    std::vector<Point2i> points;
    points.emplace_back(0, 0); points.emplace_back(3, -1);
    points.emplace_back(4, 1); points.emplace_back(2, 3);
    points.emplace_back(1, 8); points.emplace_back(-1, 7);
    points.emplace_back(-2, 8); points.emplace_back(-5, 7);
    points.emplace_back(-3, 5); points.emplace_back(-4, 2);
    points.emplace_back(-5, 4); points.emplace_back(-6, 0);
    points.emplace_back(-3, -3); points.emplace_back(0, -2);
    points.emplace_back(1, -3);
    write_to_file("points1.txt", points.begin(), points.end(), false);
    DCEL2i dcel(points);
    monotone_decompose(points, dcel);
    dcel.write_to_file("decomposeddcel1.txt");
    EXPECT_TRUE(dcel.faces.size() == 6);
}

TEST(TestTriangulation, MonotoneDecompose2) {
    std::vector<Point2i> points;
    points.emplace_back(0, 0); points.emplace_back(1, -3);
    points.emplace_back(3, -1); points.emplace_back(5, -8);
    points.emplace_back(7, -4); points.emplace_back(9, -9);
    points.emplace_back(11, -6); points.emplace_back(13, -10);
    points.emplace_back(18, -5); points.emplace_back(15, -7);
    points.emplace_back(17, 1); points.emplace_back(16, 7);
    points.emplace_back(14, 4); points.emplace_back(12, 6);
    points.emplace_back(10, 2); points.emplace_back(8, 8);
    points.emplace_back(6, 3); points.emplace_back(4, 5);
    points.emplace_back(1, -2);
    write_to_file("points2.txt", points.begin(), points.end(), false);
    DCEL2i dcel(points);
    monotone_decompose(points, dcel);
    dcel.write_to_file("decomposeddcel2.txt");
    EXPECT_TRUE(dcel.faces.size() == 10);
}

