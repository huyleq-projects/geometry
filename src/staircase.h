#ifndef STAIRCASE_H
#define STAIRCASE_H

#include "geometry.h"
#include <iostream>
#include <vector>
#include <algorithm>

// Let P be a set of points in the plane. A point p in P is Pareto-optimal if no other point
// in P is both above and to the right of p. The Pareto-optimal points can be connected by
// horizontal and vertical lines into the staircase of P, with a Pareto-optimal point at the top
// right corner of every step.

// stairpoints are stored in decreasing x
template <typename T>
bool valid_staircase(const std::vector<Point<T>> &stairpoints,
                     const std::vector<Point<T>> &points) {
    for (auto p: points) {
        bool check = false;
        for (auto it = stairpoints.rbegin(); it != stairpoints.rend() && !check; it++) {
            if ((*it) == p) check = true; // p is a points in staircase
            else if (it->x >= p.x) { // first point in staircase whose x is not smaller than p.x
                if (it->y >= p.y) check = true; // valid point
                else return false;
            }
        }
        if (!check) return false; // all stairpoints have xs smaller than point.x
    }
    return true;
}

template <typename T>
std::vector<Point<T>> staircase(const std::vector<Point<T>> &v) {
    std::vector<Point<T>> points(v.begin(), v.end());
    std::sort(points.begin(), points.end(), [](const Point<T> &a, const Point<T> &b) {
        return (a.x > b.x || (a.x == b.x && a.y < b.y));
    });

    std::vector<Point<T>> stairpoints;
    stairpoints.push_back(points[0]);

    for (auto it = points.begin()+1; it != points.end(); it++) {
        auto jt = stairpoints.back();
        if (it->x == jt.x) jt.y = it->y;
        else if (it->y > jt.y) stairpoints.push_back(*it);
    }
    std::cout << "starcase has " << stairpoints.size() << " points\n";
    return stairpoints;
}

#endif
