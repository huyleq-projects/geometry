#include "geomutil.h"
#include "intersection.h"
#include <gtest/gtest.h>

bool test_anysegmentsintersect(std::vector<Segment2i> &segments){
    write_to_file("inputsegments.txt", segments.begin(), segments.end());
    auto out = any_segments_intersect(segments);
    if (out.first && out.second)
        std::cout << (*out.first) << " intersects " << (*out.second) << std::endl;
    bool check = ((!out.first) && (!out.second)) || segments_intersect(*out.first, *out.second);
//    if (!check) write_to_file("inputsegments.txt", segments.begin(), segments.end());
    return check;
}

bool test_rectify(std::vector<Segment2i> &segments){
    write_to_file("inputsegments.txt", segments.begin(), segments.end());
    auto out = rectify(segments);

    if (out.size() != segments.size()) {
        std::cout << "output size differs from input size\n";
        return false;
    }

    if (out.back().second != 0) return false;

    std::cout << "segment " << (*out.front().first) << " has height " << out.front().second << std::endl;
    for (auto it = out.begin()+1; it != out.end(); it++) {
        std::cout << "segment " << (*it->first) << " has height " << it->second << std::endl;
        auto jt = it-1;
        if (jt->second > it->second) return false;
    }
    return true;
}

TEST(TestIntersection, RectifyInputFile) {
    std::vector<Segment2i> segments = read_segments_from_file<int>("../data/inputsegments4.txt");
    EXPECT_TRUE(test_rectify(segments));
}

TEST(TestIntersection, AnySegmentsIntersectInputFile) {
    std::vector<Segment2i> segments = read_segments_from_file<int>("../data/inputsegments.txt");
    EXPECT_TRUE(test_anysegmentsintersect(segments));
}

TEST(TestIntersection, AnySegmentsIntersectRandom) {
    std::vector<Segment2i> segments = random_test_segments();
    EXPECT_TRUE(test_anysegmentsintersect(segments));
}

TEST(TestIntersection, PreprocessSegments2) {
    std::vector<Segment2i> segments = read_segments_from_file<int>("../data/inputsegments.txt");
    BSTNode<Event2<int>> *segmentq = preprocess_segments2(segments);
    EXPECT_TRUE(size(segmentq) == 8);
    inorder_walk(segmentq);
    clean_data(segmentq);
}
