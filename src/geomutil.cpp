#include <chrono>
#include <random>
#include <algorithm>

#include "geomutil.h"

// return a vector of n random integer points
std::vector<Point2i> random_ipoints(int n, int a, int b) {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(a, b);
    std::vector<Point2i> v(n);
    for (Point2i &p: v) {
        p.x = distribution(generator); p.y = distribution(generator);
    }
    return v;
}

std::vector<Point2i> random_test_points(int n) {
    // make a random list of points
    if (n == 0) {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine generator(seed);
        std::uniform_int_distribution<int> distribution(3, 10000);
        n = distribution(generator);
    }
    std::vector<Point2i> vp = random_ipoints(n, -1e6, 1e6);
    std::cout << "generated " << n << " points\n";
    return vp;
}

// return a vector of n random integer segments
std::vector<Segment2i> random_isegments(int n, int a, int b) {
    unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
    std::default_random_engine generator(seed);
    std::uniform_int_distribution<int> distribution(a, b);
    std::vector<Segment2i> v(n);
    for (Segment2i &s: v) {
        s.p1.x = distribution(generator); s.p1.y = distribution(generator);
        s.p2.x = distribution(generator); s.p2.y = distribution(generator);
    }
    return v;
}

// return a random polygon
std::vector<Point2i> random_ipolygon(int n) {
    std::vector<Point2i> points = random_test_points(n);
    Point2i p = *std::min_element(points.begin(), points.end(), compare_lexi<int>);
    sort_a(p, points.begin(), points.end());
    return points;
}

std::vector<Segment2i> random_test_segments(int n) {
    // make a random list of segments
    if (n == 0) {
        unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
        std::default_random_engine generator(seed);
        std::uniform_int_distribution<int> distribution(3, 10000);
        n = distribution(generator);
    }
    std::vector<Segment2i> vp = random_isegments(n, -1e6, 1e6);
    std::cout << "generated " << n << " segments\n";
    return vp;
}

