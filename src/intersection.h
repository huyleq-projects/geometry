#ifndef INTERSECTION_H
#define INTERSECTION_H

#include "geomutil.h"
#include "bst.h"
#include <stdexcept>
#include <algorithm>

// code and data structures for segment intersections
template<typename T>
struct Event {
    Event() {}
    Event(const Point<T> &q): p(q), right(-1), index(-1) {}
    Event(const Point<T> &q, int i): p(q), index(i), right(-1) {}
    Event(const Point<T> &q, int i, int r): p(q), index(i), right(r) {}
    Event(const Event &e): p(e.p), index(e.index), right(e.right) {}
    Event &operator=(const Event &e) {
        p = e.p; index = e.index; right = e.right;
        return *this;
    }

    Point<T> p; // segment endpoint
    int index; // segment index
    int right; // = 0 if left endpoint and 1 if right endpoint
};

template<typename T>
struct Event2 {
    Event2() {}
    Event2(const Point<T> &q): p(q) {}
    Event2(const Point<T> &q,
           const std::vector<int> &ls,
           const std::vector<int> &rs,
           const std::vector<int> &cs):
           p(q), left_segments(ls), right_segments(rs), cross_segments(cs) {}
    Event2(const Event2 &e): p(e.p),
                             left_segments(e.left_segments),
                             right_segments(e.right_segments),
                             cross_segments(e.cross_segments) {}
    Event2 &operator=(const Event2 &e) {
        p = e.p;
        left_segments = e.left_segments;
        right_segments = e.right_segments;
        cross_segments = e.cross_segments;
        return *this;
    }

    Point<T> p; // segment endpoint
    std::vector<int> left_segments; // segments on the left of this events
    std::vector<int> right_segments; // segments on the right of this events
    std::vector<int> cross_segments; // segments crossing this events
};

template<typename T>
std::ostream &operator<<(std::ostream &os, const Event<T> &e) {
    os << "point " << e.p << " index " << e.index << " right " << e.right;
    return os;
}

template<typename T>
std::ostream &operator<<(std::ostream &os, const Event2<T> &e) {
    os << "point " << e.p << "\nleft_segments\n";
    print(e.left_segments.begin(), e.left_segments.end());
    os << "right_segments\n";
    print(e.right_segments.begin(), e.right_segments.end());
    os << "cross_segments\n";
    print(e.cross_segments.begin(), e.cross_segments.end());
    return os;
}

template<typename T>
bool cmp_events(const Event<T> &e1, const Event<T> &e2) {
    return (e1.p.x < e2.p.x || (e1.p.x == e2.p.x && e1.right < e2.right) ||
           (e1.p.x == e2.p.x && e1.right == e2.right && e1.p.y < e2.p.y));
}

template<typename T>
int cmp_events2(const Event2<T> &e1, const Event2<T> &e2) {
    if (e1.p.x < e2.p.x || (e1.p.x == e2.p.x && e1.p.y < e2.p.y))
        return -1; // e1 < e2
    else if (e1.p.x == e2.p.x && e1.p.y == e2.p.y)
        return 0; // e1 = e2
    else return 1; // e1 > e2
}

template<typename T>
std::vector<Event<T>> preprocess_segments(const std::vector<Segment<T>> &v) {
    int n = v.size();
    std::vector<Event<T>> ve(2*n);
    for (int i = 0, j = 0, k = 1; i < n; i++, j += 2, k += 2) {
        ve[j].p = v[i].p1; ve[j].index = i; // p1 stored at j
        ve[k].p = v[i].p2; ve[k].index = i; // p2 stored at k

        if (v[i].p1.x < v[i].p2.x || (v[i].p1.x == v[i].p2.x && v[i].p1.y < v[i].p2.y)) {
            ve[j].right = 0; ve[k].right = 1; // p1 is left, p2 is right
        }
        else if (v[i].p1.x > v[i].p2.x || (v[i].p1.x == v[i].p2.x && v[i].p1.y > v[i].p2.y)) {
            ve[j].right = 1; ve[k].right = 0; // p1 is right, p2 is left
        }
        else throw std::runtime_error("degenerate segment: a point");
    }
    return ve;
}

template<typename T>
BSTNode<Event2<T>> *preprocess_segments2(const std::vector<Segment<T>> &v) {
    int n = v.size();
    BSTNode<Event2<T>> *node = nullptr;
    for (int i = 0; i < n; i++) {
        Point<T> left, right;
        if (v[i].p1.x < v[i].p2.x || (v[i].p1.x == v[i].p2.x && v[i].p1.y < v[i].p2.y)) {
            left = v[i].p1; right = v[i].p2;
        }
        else if (v[i].p1.x > v[i].p2.x || (v[i].p1.x == v[i].p2.x && v[i].p1.y > v[i].p2.y)) {
            left = v[i].p2; right = v[i].p1;
        }
        else throw std::runtime_error("degenerate segment: a point");

        // process left end point
        Event2<T> *eleft = new Event2<T>(left);
        BSTNode<Event2<T>> *x = search(node, eleft, cmp_events2<T>); // search if this event is already in
        if (!x) x = insert(node, eleft, cmp_events2<T>); // if not, insert it in
        x->data->right_segments.push_back(i); // segment i is on the right of this event

        // process right end point
        Event2<T> *eright = new Event2<T>(right);
        x = search(node, eright, cmp_events2<T>); // search if this event is already in
        if (!x) x = insert(node, eright, cmp_events2<T>); // if not, insert it in
        x->data->left_segments.push_back(i); // segment i is on the left of this event
    }
    return node; // remember to clean
}

template<typename T>
std::pair<Segment<T>*, Segment<T>*>
any_segments_intersect(std::vector<Segment<T>> &segments) {
    std::pair<Segment<T>*,Segment<T>*> out(nullptr, nullptr); // result
    std::vector<Event<T>> events = preprocess_segments<T>(segments); // to events
    std::sort(events.begin(), events.end(), cmp_events<T>); // sort events
    BSTNode<Segment<T>> *segment_treehead = nullptr; // to store comparable segments

    for (const Event<T> &event: events) {
        Segment<T> *current_segment = &segments[event.index]; // current event

        // compare ab and cd by y coordinates of intersections with vertical line at x
        auto cmp = [x = double(event.p.x)] (const Segment<T> &ab, const Segment<T> &cd) {
            double y_ab = (ab.p1.x == ab.p2.x)? std::min(ab.p1.y, ab.p2.y): colinear_y(ab, x);
            double y_cd = (cd.p1.x == cd.p2.x)? std::min(cd.p1.y, cd.p2.y): colinear_y(cd, x);
            return y_ab - y_cd;
        };

        if (event.right) { // right endpoint, delete
            BSTNode<Segment<T>> *node = search(segment_treehead, current_segment, cmp);
            BSTNode<Segment<T>> *succ = successor(segment_treehead, node);
            BSTNode<Segment<T>> *pred = predecessor(segment_treehead, node);

            if (succ && pred && segments_intersect(*succ->data, *pred->data)) {
                out.first = succ->data; out.second = pred->data;
                clean(segment_treehead);
                return out;
            }

            del(segment_treehead, node);
        }
        else { // left endpoint, insert
            BSTNode<Segment<T>> *node = insert(segment_treehead, current_segment, cmp);
            BSTNode<Segment<T>> *succ = successor(segment_treehead, node);
            BSTNode<Segment<T>> *pred = predecessor(segment_treehead, node);

            if (succ && segments_intersect(*current_segment, *succ->data)) {
                out.first = current_segment; out.second = succ->data;
                clean(segment_treehead);
                return out;
            }

            if (pred && segments_intersect(*current_segment, *pred->data)) {
                out.first = current_segment; out.second = pred->data;
                clean(segment_treehead);
                return out;
            }
        }
    }

    clean(segment_treehead);
    return out;
}

template<typename T>
using SegmentWithHeight = std::pair<Segment<T>*, int>;

template<typename T>
std::vector<SegmentWithHeight<T>>
rectify(std::vector<Segment<T>> &segments) {
    std::vector<SegmentWithHeight<T>> out; // result
    std::vector<Event<T>> events = preprocess_segments<T>(segments); // to events
    std::sort(events.begin(), events.end(), cmp_events<T>); // sort events
    BSTNode<Segment<T>> *segment_treehead = nullptr; // to store comparable segments

    for (const Event<T> &event: events) {
        Segment<T> *current_segment = &segments[event.index]; // current event

        // compare ab and cd by y coordinates of intersections with vertical line at x
        auto cmp = [x = double(event.p.x)] (const Segment<T> &ab, const Segment<T> &cd) {
            double y_ab = (ab.p1.x == ab.p2.x)? std::min(ab.p1.y, ab.p2.y): colinear_y(ab, x);
            double y_cd = (cd.p1.x == cd.p2.x)? std::min(cd.p1.y, cd.p2.y): colinear_y(cd, x);
            return y_ab - y_cd;
        };

        if (event.right) { // right endpoint
            BSTNode<Segment<T>> *node = search(segment_treehead, current_segment, cmp);
            del(segment_treehead, node);
        }
        else { // left endpoint
            BSTNode<Segment<T>> *node = insert(segment_treehead, current_segment, cmp);
            BSTNode<Segment<T>> *pred = predecessor(segment_treehead, node);
            if (pred) {
                Segment<T> *pred_segment = pred->data;
                auto it = out.begin();
                while (it->first != pred_segment) {
                    it->second--; it++;
                }
                int height = it->second--;
                auto x = std::make_pair(current_segment, height);
                out.insert(it+1, x);
            }
            else if (out.empty()) out.emplace_back(current_segment, 0);
            else {
                int height = out.front().second - 1;
                auto x = std::make_pair(current_segment, height);
                out.insert(out.begin(), x);
            }
        }
    }

    clean(segment_treehead);
    return out;
}



#endif
