#include "dcel.h"
#include "triangulation.h"
#include <cassert>
#include <iostream>

int main(int argc, char **argv) {
    std::vector<Point2i> points;
    points.emplace_back(0, 0); points.emplace_back(3, -1);
    points.emplace_back(4, 1); points.emplace_back(2, 3);
    points.emplace_back(1, 8); points.emplace_back(-1, 7);
    points.emplace_back(-2, 8); points.emplace_back(-5, 7);
    points.emplace_back(-3, 5); points.emplace_back(-4, 2);
    points.emplace_back(-5, 4); points.emplace_back(-6, 0);
    points.emplace_back(-3, -3); points.emplace_back(0, -2);
    points.emplace_back(1, -3);
    write_to_file("points.txt", points.begin(), points.end());
    DCEL2i dcel(points);
    monotone_decompose(points, dcel);
    std::cout << dcel << std::endl;
    dcel.write_to_file("decomposeddcel.txt");
    return 0;
}
